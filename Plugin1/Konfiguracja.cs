﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Plugin1
{
    public class Konfiguracja
    {
        public bool Wybrano;
        [System.Xml.Serialization.XmlElement("KonfiguracjaXL")]
        public KonfiguracjaXL KonfiguracjaXL;
        [System.Xml.Serialization.XmlElement("Program")]
        public Program Program;
        [System.Xml.Serialization.XmlElement("Rejestry")]
        public Rejestry Rejestry;

        public static Konfiguracje _konfiguracje;



        public static string programID = "";
        public static string baza = "";
        public static string opeIdent = "";
        public static string opeHaslo = "";
        public static string serwerKlucza = "";


        public static string XLNumerDokumentu_kolumna = "";
        public static bool pokazdokument = false;
        public static string KontekstOperatoraId;
        public static bool WylogujXlApi = true;

        public static bool fwczytanokonfiguracjelokalna = false;

        public static List<Rejestr> ListaRejestrow = null;
        //private static string _connectionstring = BuildConnectionString();

        //private static PolaczenieSql laczeSQL = null;
        //public static PolaczenieSql LaczeSQL
        //{
        //    set
        //    {
        //        laczeSQL = value;
        //    }
        //    get
        //    {
        //        return laczeSQL;
        //    }
        //}



        //public static void OdswiezPolaczenie()
        //{
        //    try
        //    {
        //        _connectionstring = BuildConnectionString();
        //        LaczeSQL = new PolaczenieSql(_connectionstring);
        //    }
        //    catch 
        //    {
        //        Other.PokazKomunikat("Nie można połączyć się z bazą. Proszę sprawdzić konfigurację połączenia.");
        //    }
        //}
        
        //public static int WczytajKonfiguracje()
        //{
        //    try
        //    {
        //        //string PathKonfiguracjaXml = Application.StartupPath + "\\PluginIED2XL.xml";
        //        //Konfiguracje konfiguracje = new Konfiguracje();
        //        //XmlSerializer des = new XmlSerializer(typeof(Konfiguracje));
        //        //TextReader tr = new StreamReader(PathKonfiguracjaXml);
        //        //konfiguracje = (Konfiguracje)des.Deserialize(tr);
        //        //_konfiguracje = konfiguracje;
        //        //tr.Close();
        //        //bool f = false;
        //        //foreach (Konfiguracja k in konfiguracje.ListaKonfiguracji)
        //        //{
        //        //    if (k.Wybrano == true)
        //        //    {
        //        //        if (k.KonfiguracjaXL != null)
        //        //        {
        //        //            programID = k.KonfiguracjaXL.programID;
        //        //            baza = k.KonfiguracjaXL.baza;
        //        //            opeIdent = k.KonfiguracjaXL.opeIdent;
        //        //            opeHaslo = k.KonfiguracjaXL.opeHaslo;
        //        //            serwerKlucza = k.KonfiguracjaXL.serwerKlucza;
        //        //        }
        //        //        if (k.Program != null)
        //        //        {
        //        //            pokazdokument = Convert.ToBoolean(k.Program.PokazDokument.ToString());
        //        //            KontekstOperatoraId = k.Program.KontekstOperatoraId;
        //        //            WylogujXlApi = Convert.ToBoolean(k.Program.WylogujXlApi.ToString());

        //        //            fwczytanokonfiguracjelokalna = true;
        //        //        }
        //        //        if (k.Rejestry != null)
        //        //        {
        //        //            ListaRejestrow = new List<Rejestr>();
        //        //            foreach (Rejestr r in k.Rejestry.ListaRejestry)
        //        //            {
        //        //                Rejestr trej = new Rejestr();
        //        //                trej.Nazwa = r.Nazwa;
        //        //                trej.Seria = r.Seria;
        //        //                trej.StawkaVAT = r.StawkaVAT;
        //        //                trej.RejestrXL = r.RejestrXL;
        //        //                trej.FormaPlatnosci = r.FormaPlatnosci;
        //        //                trej.FormaPlatnosci_kolumna = r.FormaPlatnosci_kolumna;
        //        //                trej.Data_kolumna = r.Data_kolumna;
        //        //                trej.DataSpr_kolumna = r.DataSpr_kolumna;
        //        //                trej.DataVat_kolumna = r.DataVat_kolumna;
        //        //                trej.Termin_kolumna = r.Termin_kolumna;
        //        //                trej.XLNumerDokumentu_kolumna = r.XLNumerDokumentu_kolumna;
        //        //                trej.Opis_kolumna = r.Opis_kolumna;

        //        //                ListaRejestrow.Add(trej);
        //        //            }
        //        //        }
        //        //            //seria = k..Seria;
        //        //            //stawkaVAT = k.KonfiguracjaProgram.StawkaVAT;

        //        //        f = true;
        //        //        break;
        //        //    }
        //        //}
        //        //if (!f)
        //        //{
        //        //    //Other.PokazKomunikat("Nie wczytano konfiguracji!");
        //        //    return -1;
        //        //}
        //    }
        //    catch
        //    {
        //        //Other.PokazKomunikat("Błąd! Nie wczytano konfiguracji.");
        //        //return -1;
        //    }
        //    //_connectionstring = BuildConnectionString();
        //    return 0;
        //}

        public static int WczytajKonfiguracje(string xml)
        {
            try
            {
                Konfiguracje konfiguracje = new Konfiguracje();
                XmlSerializer des = new XmlSerializer(typeof(Konfiguracje));
                TextReader tr = new StringReader(xml);
                konfiguracje = (Konfiguracje)des.Deserialize(tr);
                _konfiguracje = konfiguracje;
                tr.Close();
                bool f = false;
                foreach (Konfiguracja k in konfiguracje.ListaKonfiguracji)
                {
                    if (k.Wybrano == true)
                    {
                        if (k.KonfiguracjaXL != null)
                        {
                            programID = k.KonfiguracjaXL.programID;
                            baza = k.KonfiguracjaXL.baza;
                            opeIdent = k.KonfiguracjaXL.opeIdent;
                            opeHaslo = k.KonfiguracjaXL.opeHaslo;
                            serwerKlucza = k.KonfiguracjaXL.serwerKlucza;
                        }
                        if (k.Program != null)
                        {
                            pokazdokument = Convert.ToBoolean(k.Program.PokazDokument.ToString());
                            KontekstOperatoraId = k.Program.KontekstOperatoraId;
                            WylogujXlApi = Convert.ToBoolean(k.Program.WylogujXlApi.ToString());

                            fwczytanokonfiguracjelokalna = true;
                        }
                        if (k.Rejestry != null)
                        {
                            ListaRejestrow = new List<Rejestr>();
                            foreach (Rejestr r in k.Rejestry.ListaRejestry)
                            {
                                Rejestr trej = new Rejestr();
                                trej.Nazwa = r.Nazwa;
                                trej.TypDokumentuXl = r.TypDokumentuXl;
                                trej.PozycjaOpis_wymiar = r.PozycjaOpis_wymiar;
                                trej.PozycjaJm = r.PozycjaJm;
                                trej.PozycjaStawkaVAT_wymiar = r.PozycjaStawkaVAT_wymiar;
                                trej.Seria = r.Seria;
                                trej.StawkaVAT = r.StawkaVAT;
                                trej.RejestrXL = r.RejestrXL;
                                trej.RejestrXL_kolumna = r.RejestrXL_kolumna;
                                trej.FormaPlatnosci = r.FormaPlatnosci;
                                trej.FormaPlatnosci_kolumna = r.FormaPlatnosci_kolumna;
                                trej.Data_kolumna = r.Data_kolumna;
                                trej.DataSpr_kolumna = r.DataSpr_kolumna;
                                trej.DataVat_kolumna = r.DataVat_kolumna;
                                trej.Termin_kolumna = r.Termin_kolumna;
                                trej.XLNumerDokumentu_kolumna = r.XLNumerDokumentu_kolumna;
                                trej.Opis_kolumna = r.Opis_kolumna;
                                if (!String.IsNullOrEmpty(r.TabelaVat_kolumna))
                                    trej.TabelaVat_kolumna = r.TabelaVat_kolumna;
                                if (!String.IsNullOrEmpty(r.Kurs_kolumna))
                                    trej.Kurs_kolumna = r.Kurs_kolumna;
                                trej.MapowanieFormaPlatnosci = r.MapowanieFormaPlatnosci;
                                trej.OpisAnalityczny = r.OpisAnalityczny;
                                ListaRejestrow.Add(trej);
                            }
                        }
                        //seria = k..Seria;
                        //stawkaVAT = k.KonfiguracjaProgram.StawkaVAT;

                        f = true;
                        break;
                    }
                }
                if (!f)
                {
                    //Other.PokazKomunikat("Nie wczytano konfiguracji!");
                    return -1;
                }
            }
            catch
            {
                //Other.PokazKomunikat("Błąd! Nie wczytano konfiguracji.");
                return -1;
            }
            //_connectionstring = BuildConnectionString();
            return 0;
        }

        public static int ZapiszKonfiguracje()
        {
            try
            {
   
                _konfiguracje.ListaKonfiguracji[0].KonfiguracjaXL.opeIdent = opeIdent;
                _konfiguracje.ListaKonfiguracji[0].KonfiguracjaXL.opeHaslo = opeHaslo;
                _konfiguracje.ListaKonfiguracji[0].KonfiguracjaXL.baza = baza;
                _konfiguracje.ListaKonfiguracji[0].KonfiguracjaXL.serwerKlucza = serwerKlucza;
                _konfiguracje.ListaKonfiguracji[0].KonfiguracjaXL.programID = programID;

                //_konfiguracje.ListaKonfiguracji[0].KonfiguracjaProgram.Seria = seria;
                //_konfiguracje.ListaKonfiguracji[0].StawkaVAT = stawkaVAT;
                
                XmlSerializer writer = new XmlSerializer(typeof(Konfiguracje));
                System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath+ "\\Plugin1.xml");
                writer.Serialize(file, _konfiguracje);
                file.Close();          
            }
            catch
            {
               // Other.PokazKomunikat("Błąd zapisu konfiguracji.");
                return -1;
            }
            return 0;
        }

        //private static string BuildConnectionString()
        //{
        //    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        //    builder.DataSource = _datasource;
        //    builder.InitialCatalog = _database;
        //    builder.PersistSecurityInfo = true;
        //    builder.UserID = _login;
        //    builder.Password = _pass;
        //    return builder.ToString();
        //}

    }

    public class Konfiguracje
    {
        [System.Xml.Serialization.XmlElement("Konfiguracja")]
        public Konfiguracja[] ListaKonfiguracji;
    }
    public class KonfiguracjaXL
    {
        public string programID;
        public string baza;
        public string opeIdent;
        public string opeHaslo;
        public string serwerKlucza;
    }
    public class Program
    {
        public string PokazDokument;
        public string KontekstOperatoraId;
        public string WylogujXlApi;
    }
    public class Rejestry
    {
        [System.Xml.Serialization.XmlElement("Rejestr")]
        public Rejestr[] ListaRejestry;
    }
    public class Rejestr 
    {
        public string Nazwa;
        public string TypDokumentuXl;//AFZ,FZ
        public string PozycjaOpis_wymiar;
        public string PozycjaJm;
        public string PozycjaStawkaVAT_wymiar;
        public string Seria;
        public string RejestrXL;
        public string RejestrXL_kolumna;
        public string StawkaVAT;
        public string FormaPlatnosci;
        public string FormaPlatnosci_kolumna;
        public string Data_kolumna;
        public string DataSpr_kolumna;
        public string DataVat_kolumna;
        public string Termin_kolumna;
        public string XLNumerDokumentu_kolumna;
        public string Opis_kolumna;
        public string Kurs_kolumna;
        public string TabelaVat_kolumna;
        public string MapowanieFormaPlatnosci;
        public string OpisAnalityczny;//TAK,NIE,PROBA
    }


    public class Parametr
    {
        public string Nazwa { get; set; }
        public object Wartosc { get; set; }
        public Parametr(string nazwa, object wartosc)
        {
            Nazwa = nazwa;
            Wartosc = wartosc;
        }
        public Parametr() { }
    }

  
}