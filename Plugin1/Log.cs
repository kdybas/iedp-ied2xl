﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Plugin1
{
    public class Log
    {
        private static string _logtext = "";

        public static void Zapisz()
        {
            //System.IO.File.AppendAllText(Application.StartupPath + "\\PluginIED2XL_log.txt", _logtext);

            System.IO.File.AppendAllText(Application.StartupPath + "\\Log_PluginIED2XL.txt", _logtext);
    
        }

        public static void Dodaj(string tekst)
        {
            try
            {
                _logtext = Environment.NewLine;
                _logtext += "[" + DateTime.Now.ToString() + "] ";
                _logtext += tekst;
                Zapisz();
            }
            catch { }
        }
        //System.IO.File.WriteAllText(@"C:\Users\Public\TestFolder\WriteText.txt", text);
    }
}
