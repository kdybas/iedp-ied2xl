﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Plugin1
{
    public class Other
    {
        public static Nullable<double> PodajWartoscDouble(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName))
                return null;
            else
                return Convert.ToDouble(dr[columnName].ToString());
        }

        public static Nullable<int> PodajWartoscInt(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName))
                return null;
            else
                return Convert.ToInt32(dr[columnName].ToString());
        }

        public static Nullable<DateTime> PodajWartoscDateTime(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName))
                return null;
            else
                return Convert.ToDateTime(dr[columnName].ToString());
        }

        public static string PodajWartoscString(DataRow dr, string columnName)
        {
            if (dr.IsNull(columnName))
                return null;
            else
                return dr[columnName].ToString();
        }

        public static SqlParameter GetParameter(string name, Nullable<double> value, SqlDbType type)
        {
            if (value != null)
                return new SqlParameter(name, value);
            else
            {
                SqlParameter sqlPar = new SqlParameter(name, type);
                sqlPar.SqlValue = DBNull.Value;
                return sqlPar;
            }
        }

        public static SqlParameter GetParameter(string name, Nullable<DateTime> value, SqlDbType type)
        {
            if (value != null)
                return new SqlParameter(name, value);
            else
            {
                SqlParameter sqlPar = new SqlParameter(name, type);
                sqlPar.SqlValue = DBNull.Value;
                return sqlPar;
            }
        }

        public static SqlParameter GetParameter(string name, Nullable<int> value, SqlDbType type)
        {
            if (value != null)
                return new SqlParameter(name, value);
            else
            {
                SqlParameter sqlPar = new SqlParameter(name, type);
                sqlPar.SqlValue = DBNull.Value;
                return sqlPar;
            }
        }

        public static SqlParameter GetParameter(string name, string value, SqlDbType type)
        {
            if (value != null)
                return new SqlParameter(name, value);
            else
            {
                SqlParameter sqlPar = new SqlParameter(name, type);
                sqlPar.SqlValue = DBNull.Value;
                return sqlPar;
            }
        }

        public static void UstawOkno(Object okno)
        {
            ((Form)okno).FormBorderStyle = FormBorderStyle.None;
            if (((Form)okno).Owner == null)
                ((Form)okno).StartPosition = FormStartPosition.CenterScreen;
            else
                ((Form)okno).StartPosition = FormStartPosition.CenterParent;
            ((Form)okno).Width = 240;
            ((Form)okno).Height = 320;
            ((Form)okno).WindowState = FormWindowState.Normal;
            ((Form)okno).ShowIcon = false;
            ((Form)okno).ShowInTaskbar = false;     
        }

        public static void PokazKomunikat(string komunikat)
        {
            //using (KomunikatForm form = new KomunikatForm(komunikat))
            //{
            //    form.StartPosition = FormStartPosition.CenterParent;
            //    form.ShowDialog();
            //}
            MessageBox.Show(komunikat, "");
        }

        //public static bool PokazKomunikatTakNie(string komunikat)
        //{
        //    using (WyborForm form = new WyborForm(komunikat))
        //    {
        //        form.StartPosition = FormStartPosition.CenterParent;
        //        form.ShowDialog();
        //        if (form.DialogResult == DialogResult.Yes)
        //            return true;
        //        else
        //            return false;
        //    }
        //}

        public static string GetLocalIP()
        {
            string _IP = null;

            // Resolves a host name or IP address to an IPHostEntry instance.
            // IPHostEntry - Provides a container class for Internet host address information.
            System.Net.IPHostEntry _IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

            // IPAddress class contains the address of a computer on an IP network.
            foreach (System.Net.IPAddress _IPAddress in _IPHostEntry.AddressList)
            {
                // InterNetwork indicates that an IP version 4 address is expected
                // when a Socket connects to an endpoint
                if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    _IP = _IPAddress.ToString();
                }
            }
            return _IP;
        }

        public static void ArchiwizujLog() 
        {
            
        }

        //public static string NumerDokumentuXl(string GITNumer, string GITTyp)
        //{
        //    return Konfiguracja.LaczeSQL.NumerDokumentuXl(GITNumer, GITTyp);
        //}

        public static string WyszukajMapowanie(string Klucz, string KonfiguracjaMapowanie)
        {
            string[] kmparts = KonfiguracjaMapowanie.Split(new char[] { ',', ';' });
            foreach (string kmpart in kmparts) 
            {
                string[] valparts = kmpart.Split(new char[] { '=' });
                if (valparts.Length >= 2) 
                {
                    if (valparts[0] == Klucz)
                    {
                        return valparts[1];
                    }
                }
            }
            return "";
        }
  
    }

    public enum ObiektStan
    {
        ISTNIEJE = 0,
        USUNIETY = 1,
        NIEOKRESLONY = -1
    }
}
