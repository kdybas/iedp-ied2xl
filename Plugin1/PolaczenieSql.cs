﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Plugin1
{
    public class PolaczenieSql
    {
        private string _connectionString;
        private SqlConnection _sqlConnetion;
        private SqlCommand _sqlCommand;

        public PolaczenieSql(string connectionstring) 
        {
            OtworzPolaczenie(connectionstring);
        }

        private void OtworzPolaczenie(string connectionString)
        {
            try
            {
                this._connectionString = connectionString;
                this._sqlConnetion = new SqlConnection(this._connectionString);
                this._sqlCommand = _sqlConnetion.CreateCommand();
                this._sqlConnetion.Open();

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Błąd połączenia : {0}", ex.Message));
            }
        }

        public void ZamknijPolaczenie()
        {
            try
            {
                _sqlConnetion.Close();
                _sqlConnetion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Błąd zamykania połączenia: {0}", ex.Message));
            }
        }

        private void OdswiezPolaczenie()
        {
            if (_sqlConnetion.State == ConnectionState.Closed) _sqlConnetion.Open();
        }

        public bool SprawddzPolaczenie() 
        {
            try 
            {
                OdswiezPolaczenie();
                if (!String.IsNullOrEmpty(_connectionString))
                    return true;
            }
            catch (Exception e) 
            {
                return false;
            }
            return false;
        }

        #region SQL_Command

        //funkcja zwracająca obiekt DataView
        private DataView SelectDataView(SqlCommand cmd)
        {
            OdswiezPolaczenie();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.DefaultView;
        }

        //funkcja zwracająca obiekt DataTable      
        private DataTable SelectDataTable(SqlCommand cmd)
        {
            OdswiezPolaczenie();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.DefaultView.Table;
        }



        #endregion

        #region SQL_QueryString

        //funkcja zwracająca obiekt DataView
        private DataView SelectDataView(string scmd)
        {
            OdswiezPolaczenie();
            SqlDataAdapter da = new SqlDataAdapter(scmd, _sqlConnetion);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.DefaultView;
        }

        //funkcja zwracjąca obiekt DataTable
        private DataTable SelectDataTable(string scmd)
        {
            OdswiezPolaczenie();
            SqlDataAdapter da = new SqlDataAdapter(scmd, _sqlConnetion);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        #endregion

        //-----

        public string UpdateXLNumer( string nazwakolumny, string wartosc,int DpId)
        {
            string sqlUpdate = String.Format("UPDATE ingenes.DokumentyPrzychodzace set {0}=@value where Dp_ID=@Dp_ID", nazwakolumny);

            string temp = "";
            SqlTransaction sqlTransaction = null;
            try
            {
                sqlTransaction = _sqlConnetion.BeginTransaction();
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.Connection = _sqlConnetion;
                    sqlCommand.Transaction = sqlTransaction;
                  
                                
                    sqlCommand.CommandText = sqlUpdate;
                    if (!String.IsNullOrEmpty(wartosc))
                        sqlCommand.Parameters.Add(new SqlParameter("value", wartosc));
                    else
                    {
                        SqlParameter parametr = new SqlParameter("value", SqlDbType.VarChar);
                        parametr.Value = DBNull.Value;
                        sqlCommand.Parameters.Add(parametr);
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("Dp_ID", DpId));

                                
                    if (sqlCommand.ExecuteNonQuery() == 0)                                  
                        throw new Exception("");                   
                }

                //zapis uprawnień           
                sqlTransaction.Commit();
                temp = "OK";
            }
            catch (Exception e)
            {
                sqlTransaction.Rollback();
                throw new Exception("Błąd  " + e.Message);              
            }
            return temp;
        }

        public byte[] SprawdzLastUpdate(int id) 
        {
            DataView dv = SelectDataView(string.Format("select top 1 Dp_LastUpdate from ingenes.DokumentyPrzychodzace where Dp_Id={0}", id));
            byte[] Dp_LastUpdate = (byte[])dv.Table.Rows[0]["Dp_LastUpdate"];
            return Dp_LastUpdate;
        }

        SqlTransaction sqlTransactionZaksieguj = null;

        public string UpdateZaksiegujDokumentyPrzychodzace(DateTime Dp_DataZaksiegowania, int Dp_ZaksiegowalOperator, int Dp_Status, int DpId)
        {
            string sqlUpdate = String.Format("UPDATE ingenes.DokumentyPrzychodzace set Dp_DataZaksiegowania=@Dp_DataZaksiegowania, Dp_ZaksiegowalOperator=@Dp_ZaksiegowalOperator, Dp_Status=@Dp_Status where Dp_ID=@Dp_ID");

            string temp = "";

            try
            {
                sqlTransactionZaksieguj = _sqlConnetion.BeginTransaction();
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.Connection = _sqlConnetion;
                    sqlCommand.Transaction = sqlTransactionZaksieguj;


                    sqlCommand.CommandText = sqlUpdate;
                    sqlCommand.Parameters.Add(new SqlParameter("Dp_DataZaksiegowania", Dp_DataZaksiegowania));
                    sqlCommand.Parameters.Add(new SqlParameter("Dp_ZaksiegowalOperator", Dp_ZaksiegowalOperator));
                    sqlCommand.Parameters.Add(new SqlParameter("Dp_Status", Dp_Status));
                    sqlCommand.Parameters.Add(new SqlParameter("Dp_ID", DpId));


                    if (sqlCommand.ExecuteNonQuery() == 0)
                        throw new Exception("");
                }

                //zapis uprawnień           
                //sqlTransaction.Commit();
                temp = "OK";
            }
            catch (Exception e)
            {
                sqlTransactionZaksieguj.Rollback();
                throw new Exception( "Błąd  " + e.Message);          
            }
            return temp;
        }

        public string InsertZaksiegujDokumentyPrzychodzaceZdarzenia(int Dpz_DpId, int Dpz_DptzId, int Dpz_Operator, DateTime Dpz_Data, string Dpz_Opis, string Dpz_Komentarz)
        {
            string sqlUpdate = String.Format("INSERT INTO ingenes.DokumentyPrzychodzaceZdarzenia(Dpz_DpId,Dpz_DptzId,Dpz_Operator,Dpz_Data,Dpz_Opis,Dpz_Komentarz) VALUES (@Dpz_DpId,@Dpz_DptzId,@Dpz_Operator,@Dpz_Data,@Dpz_Opis,@Dpz_Komentarz)");

            string temp = "";
   
            try
            {
            
                using (SqlCommand sqlCommand = new SqlCommand())
                {
                    sqlCommand.Connection = _sqlConnetion;
                    sqlCommand.Transaction = sqlTransactionZaksieguj;


                    sqlCommand.CommandText = sqlUpdate;
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_DpId", Dpz_DpId));
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_DptzId", Dpz_DptzId));
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_Operator", Dpz_Operator));
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_Data", Dpz_Data));
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_Opis", Dpz_Opis));
                    sqlCommand.Parameters.Add(new SqlParameter("Dpz_Komentarz", Dpz_Komentarz));




                    if (sqlCommand.ExecuteNonQuery() == 0)
                        throw new Exception("");
                }

                //zapis uprawnień           
                sqlTransactionZaksieguj.Commit();
                temp = "OK";
            }
            catch (Exception e)
            {
                temp = "Błąd  " + e.Message;
                sqlTransactionZaksieguj.Rollback();
            }
            return temp;
        }

        public DataTable SprawdzIdWymiaru(string nazwawymiaru, int typ, int parId)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;

            if (typ == 0)
                sqlCommand.CommandText = String.Format("SELECT WMR_ID FROM CDN.Wymiary WHERE WMR_Nazwa='{0}' and WMR_ParId={1}", nazwawymiaru, parId);
            if (typ == 1)
                sqlCommand.CommandText = String.Format("SELECT FRS_ID FROM CDN.FrmStruktura WHERE FRS_Nazwa='{0}' AND FRS_Warstwa=2", nazwawymiaru);
            if (typ == 2)
                sqlCommand.CommandText = String.Format("SELECT SLW_ID FROM CDN.Slowniki WHERE SLW_Nazwa='{0}' AND SLW_Kategoria='Rodzaj lokalizatora'", nazwawymiaru);
            if (typ == 3)
                sqlCommand.CommandText = String.Format("SELECT KNT_GIDNumer FROM CDN.KntKarty WHERE Knt_Akronim='{0}'", nazwawymiaru);
            //if (typ == 4)
            //   sqlCommand.CommandText = String.Format("SELECT WMR_ID FROM CDN.Wymiary WHERE WMR_Nazwa='{0}'", nazwawymiaru.Replace("Kategoria.", ""));

            if (typ == 4)
                sqlCommand.CommandText = String.Format("SELECT WMR_ID FROM CDN.Wymiary WHERE WMR_Nazwa='{0}' and WMR_ParId={1}", nazwawymiaru, parId);

            if (typ == 5)
                sqlCommand.CommandText = String.Format("SELECT PRJ_ID FROM CDN.PrjStruktura WHERE PRJ_Kod='{0}'", nazwawymiaru);
            if (typ > 5 || typ < 0)
                throw new Exception("Nieprawidłowy typ wymiaru. (" + typ + ")");

            dt = SelectDataTable(sqlCommand);
            return dt;
        }

        public DataTable SprawdzRodzajWymiaru(string nazwawymiaru, ref string refZapytanie)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;


            sqlCommand.CommandText = String.Format("SELECT WMR_Typ,Wmr_Id FROM CDN.Wymiary WHERE WMR_Nazwa='{0}'", nazwawymiaru.Replace("Kategoria.", ""));
            refZapytanie = sqlCommand.CommandText;


            dt = SelectDataTable(sqlCommand);
            return dt;
        }

        public DataTable PodajLinieKosztowe(int iddok)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;
            sqlCommand.CommandText = String.Format("SELECT *,w1.wmr_nazwa Wymiar1_Nazwa,w2.wmr_nazwa Wymiar2_Nazwa,w3.wmr_nazwa Wymiar3_Nazwa,w4.wmr_nazwa Wymiar4_Nazwa,w5.wmr_nazwa Wymiar5_Nazwa FROM ingenes.DokumentyPrzychodzaceKoszty LEFT JOIN ingenes.Wymiary w1 ON dpk_Wymiar1_ID=w1.wmr_id LEFT JOIN ingenes.Wymiary w2 ON dpk_Wymiar2_ID=w2.wmr_id LEFT JOIN ingenes.Wymiary w3 ON dpk_Wymiar3_ID=w3.wmr_id LEFT JOIN ingenes.Wymiary w4 ON dpk_Wymiar4_ID=w4.wmr_id LEFT JOIN ingenes.Wymiary w5 ON dpk_Wymiar5_ID=w5.wmr_id   WHERE dpk_dpID ={0}", iddok);

            dt = SelectDataTable(sqlCommand);
            return dt;
        }

        public DataRow PodajRejestr(int dokumentId)
        {
            string sql = string.Format("select rp.* from ingenes.DokumentyPrzychodzace join ingenes.RejestryPrzychodzace rp on dp_rdpId=rdp_Id where dp_id={0}", dokumentId);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;
            sqlCommand.CommandText = sql;
            DataTable dt = SelectDataTable(sqlCommand);
            if (dt.Rows.Count > 0)
                return dt.Rows[0];
            return null;
        }

        public string PodajGuidDokumentuPrzychodzacego(int iddok)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;
            sqlCommand.CommandText = String.Format("SELECT Dp_Guid FROM ingenes.DokumentyPrzychodzace WHERE Dp_ID ={0}", iddok);
            dt = SelectDataTable(sqlCommand);
            if (dt.Rows.Count == 0)
                throw new Exception("Brak dokumentu w bazie.");
            if (dt.Rows.Count > 1)
                throw new Exception("Niejednoznazczny Dp_ID w bazie edok.");
            if (dt.Rows[0].IsNull(0))
                return "";
            return dt.Rows[0][0].ToString();
        }


        public string PodajDateObowiazkuPodatkowego(int iddok)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;
            sqlCommand.CommandText = String.Format("SELECT Dp_DataObowiazkuPodatkowego FROM ingenes.DokumentyPrzychodzace WHERE Dp_ID ={0}", iddok);
            dt = SelectDataTable(sqlCommand);
            if (dt.Rows.Count != 1)
                return null;
            if (dt.Rows[0].IsNull("Dp_DataObowiazkuPodatkowego"))
                return null;    
            return dt.Rows[0][0].ToString();
        }

        public int PodajStatusDokumentu(int iddok)
        {
            DataTable dt = new DataTable();

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = _sqlConnetion;
            sqlCommand.CommandText = String.Format("SELECT Dp_Status FROM ingenes.DokumentyPrzychodzace WHERE Dp_ID ={0}", iddok);
            dt = SelectDataTable(sqlCommand);
            if (dt.Rows.Count != 1)
                throw new Exception("Nie pobrano statusu. Nie odnaleziono dokumentu.");
            if (dt.Rows[0].IsNull("Dp_Status"))
                throw new Exception("Nie pobrano statusu. Brak wartosci.");
            return Convert.ToInt32(dt.Rows[0][0].ToString());
        }
    }
}
