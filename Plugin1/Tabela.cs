﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plugin1
{
    public class tabela
    {
        [System.Xml.Serialization.XmlElement("wiersze")]
        public Wiersze wiersze { get; set; }
    }

    public class Wiersze
    {
        [System.Xml.Serialization.XmlElement("wiersz")]
        public Wiersz[] ListaWierszy { get; set; }
    }

    public class Wiersz
    {
        //[System.Xml.Serialization.XmlElement("stawka")]
        //public Stawka stawka { get; set; }
        //[System.Xml.Serialization.XmlElement("netto")]
        //public Netto netto { get; set; }
        //[System.Xml.Serialization.XmlElement("vat")]
        //public Vat vat { get; set; }

        public string stawka
        { get; set; }

        public decimal netto
        {
            get;
            set;
            //get
            //{
            //    return Convert.ToDecimal(netto.wartosc.Replace('.', ','));
            //}
            //set
            //{
            //    netto.wartosc = value.ToString().Replace(',', '.');
            //}
        }

        private decimal? _vat;
        public decimal? vat
        {
            get
            {
                if (_vat == null || _vat == -1)
                {
                    decimal stawkad = 0;
                    if (Decimal.TryParse(stawka, out stawkad))
                    {
                        return decimal.Round(netto * (stawkad / (decimal)100), 2);
                    }
                    else 
                    {
                        return decimal.Round(_vat ?? 0, 2);
                    }
                }
                else
                    return decimal.Round(_vat ?? 0, 2);
            }
            set
            {
                _vat = value;
                //vat.wartosc = value.ToString().Replace(',', '.');
            }
        }      


       
        public decimal brutto           
        {
            get 
            {
                return netto + vat??0;                   
            }            
        }

        public string kategoria { get; set; }
    }

    #region stare
    public class tabela_stare
    {
        [System.Xml.Serialization.XmlElement("wiersze")]
        public Wiersze_stare wiersze { get; set; }
    }

    public class Wiersze_stare
    {
        [System.Xml.Serialization.XmlElement("wiersz")]
        public Wiersz_stare[] ListaWierszy { get; set; }
    }

    public class Wiersz_stare
    {
        [System.Xml.Serialization.XmlElement("stawka")]
        public Stawka stawka { get; set; }
        [System.Xml.Serialization.XmlElement("netto")]
        public Netto netto { get; set; }
        [System.Xml.Serialization.XmlElement("vat")]
        public Vat vat { get; set; }

        public int stawka_wartosc
        {
            get
            {
                return Convert.ToInt32(stawka.wartosc);
            }
            set
            {
                stawka.wartosc = value.ToString();
            }
        }

        public decimal netto_wartosc
        {
            get
            {
                return Convert.ToDecimal(netto.wartosc.Replace('.', ','));
            }
            set
            {
                netto.wartosc = value.ToString().Replace(',', '.');
            }
        }

        public decimal vat_wartosc
        {
            get
            {
                if (String.IsNullOrEmpty(vat.wartosc))
                    return decimal.Round(netto_wartosc * ((decimal)stawka_wartosc / (decimal)100), 2);
                else
                    return decimal.Round(Convert.ToDecimal(vat.wartosc.Replace('.', ',')), 2);
            }
            set
            {
                vat.wartosc = value.ToString().Replace(',', '.');
            }
        }

        public decimal brutto_wartosc
        {
            get
            {
                return netto_wartosc + vat_wartosc;
            }
        }
    }
    #endregion

    public class Stawka
    {
        [System.Xml.Serialization.XmlAttribute("nazwaWyswietlana")]
        public string nazwaWyswietlana { get; set; }
        [System.Xml.Serialization.XmlAttribute("typ")]
        public string typ { get; set; }
        [System.Xml.Serialization.XmlText]
        public string wartosc { get; set; }
    }

    public class Netto
    {
        [System.Xml.Serialization.XmlAttribute("nazwaWyswietlana")]
        public string nazwaWyswietlana { get; set; }
        [System.Xml.Serialization.XmlAttribute("typ")]
        public string typ { get; set; }
        [System.Xml.Serialization.XmlText]
        public string wartosc { get; set; }
    }

    public class Vat
    {
        [System.Xml.Serialization.XmlAttribute("nazwaWyswietlana")]
        public string nazwaWyswietlana { get; set; }
        [System.Xml.Serialization.XmlAttribute("typ")]
        public string typ { get; set; }
        [System.Xml.Serialization.XmlText]
        public string wartosc { get; set; }
    }

}
