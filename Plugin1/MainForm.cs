﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace Plugin1
{
    public partial class MainForm : Form
    {
        public MainForm(string log)
        {
            InitializeComponent();
            tbLog.Text = log;
        }

        public void Odswiez()
        {
            tbLog.SelectionStart = tbLog.Text.Length;
            tbLog.ScrollToCaret();       
            progressBar1.Refresh();
        }

        public void WyczyscLog() 
        {
            tbLog.Text = "";
            this.Refresh();
        }
    }
}
