﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using cdn_api;
using DevExpress.XtraEditors;
using Ingenes.eDokument;
using Ingenes.eDokument.Core;
using Ingenes.eDokument.Plugins;



namespace Plugin1
{
    public class MainClass : Ingenes.eDokument.Plugins.IEdokumentPlugin
    {
        string tekstLog = "";

        bool res = false;

        XL Xl = null;

        int dok = 0;

        private XLDokumentNagInfo_20163 dokumentinfo = null;

        private PolaczenieSql polaczenie;
        private PolaczenieSql polaczenie2;

        private int idtag { get { return Convert.ToInt32(Tag); } }

        private Rejestr KonfiguracjaRejestrIED = null;

        [DllImport("ClaRUN.dll")]
        public static extern void AttachThreadToClarion();

        public MainClass()
        {


        }

        public void Init()
        {
        //    Plugin1.Konfiguracja.WczytajKonfiguracje();
        //  PluginLoader.zdarzenia.DokumentPrzychodzacyKliknietoKsieguj += zdarzenia_DokumentPrzychodzacyKliknietoKsieguj;
        }

        void zdarzenia_DokumentPrzychodzacyKliknietoKsieguj(string komunikat)
        {
            ProcesKsieguj();
        }

        private MainForm _mainForm = null;


        public void Start()
        {
            _mainForm = new MainForm(tekstLog);
            _mainForm.BringToFront();

            _mainForm.Shown += (o, e) =>
                {
                    try
                    {
                        _mainForm.Refresh();
                        Application.DoEvents();
                        ProcesKsieguj();

                        //if (res)
                        //    EdokKsieguj();

                        if (!res && dok != 0)
                            WycofajXL();

                        WylogujXL();

                        Loguj("Wynik: " + (res ? "OK" : "Bald"));
                   
                        ZapiszLog();

                        _mainForm.Refresh();
                        if (res)
                        {
                            Thread.Sleep(800);
                            _mainForm.WyczyscLog();
                            _mainForm.Close();
                            _mainForm.Dispose();
                        }
                        else
                        {
                            _Message = tekstLog;
                            //_mainForm.BringToFront();
                            _mainForm.WyczyscLog();
                            _mainForm.Close();
                            _mainForm.Dispose();
                        }
                        Application.DoEvents();
                    }
                    catch (Exception ex) 
                    {
                        res = false;
                        Loguj("Błąd plugina: " + ex.Message);
                        try { ZapiszLog(); }
                        catch { }
                    }
                };

            _mainForm.ShowDialog();
            //_mainForm.Refresh();

            
        }

        public void UpdejtujNumerDokumentu(string kolumna, string wartosc)
        {
            //string pp = "";
            //try
            //{
            //    //(Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument.UpdateZalacznikGlownyId();
            //    (Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument.DodatkowePola[kolumna.ToUpper()].Value = wartosc;
            //}
            //catch (Exception und0ex)
            //{
            //    pp = und0ex.Message;
            //}
            //if (!String.IsNullOrEmpty(pp))
            //{
            //try
            //{
            //    //
            //    var dodatkowepola = (Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument.DodatkowePola;
            //    dodatkowepola[kolumna.ToUpper()].Value = wartosc;

            //    Type typ = typeof(Ingenes.eDokument.DokumentPrzychodzacy);//System.Reflection.Assembly.GetExecutingAssembly().GetType("Ingenes.eDokument.Dokumenty.FormOtworzDokumentPrzychodzacy"); //
            //    PropertyInfo property = typ.GetProperty("DodatkowePola");
            //    property.SetValue((Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument, dodatkowepola, BindingFlags.NonPublic | BindingFlags.Instance, null, null, null);
            //}
            try 
            {
                var dodatkowepola = (Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument.DodatkowePola;
                dodatkowepola[kolumna.ToUpper()].Value = wartosc;

                PropertyInfo property = typeof(Ingenes.OR.OR_DokumentyPrzychodzace).GetProperty("DodatkowePola");
                property.GetSetMethod(true).Invoke((Parent as Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy).Dokument, new object[] { dodatkowepola });
            }
            catch (Exception undex)
            {
                Loguj("Błąd aktualizacji numeru dokumentu. Błąd: " + undex.Message + Environment.NewLine /*+ " Poprzednia próba błąd: " + pp*/);
            }
            //}
        }

        public void Koniec()
        {
            try
            {
                if (_mainForm != null)
                    _mainForm.BringToFront();
            }
            catch (Exception ex) 
            { 
                
            }
            /*
            try
            {
                _mainForm = new MainForm(tekstLog);
                _mainForm.Show();
                _mainForm.Refresh();
                Loguj(tekstLog);


                int statusdokumentu = polaczenie.PodajStatusDokumentu(idtag);
                if (statusdokumentu != 7)
                {
                    Loguj("Przetwarzany dokument nie jest zaksięgowany. (Dp_Status = " + statusdokumentu + ")");
                    Xl.ZamknijDokument(dok, -1);
                    return;
                }
                if (Plugin1.Konfiguracja._konfiguracje == null)
                    throw new Exception("Nie wczytano konfiguracji.");
                if (!(dok > 0))
                    throw new Exception("Brak aktywnego dokumentu fs xl.");
                if (polaczenie == null || polaczenie2 == null)
                    throw new Exception("Brak aktywych połączeń sql");

                Xl.ZamknijDokument(dok);
                Loguj(75);
                Loguj("Dokument wygenerowany");


                int bladapi = 0;

               
            }
            catch (Exception exkon)
            {
                Loguj("Błąd: " + exkon.Message);
            }
            finally 
            {
                ZapiszLog();
                if (_mainForm != null)
                {
                    _mainForm.Refresh();
                    _mainForm.WyczyscLog();
                    Thread.Sleep(500);
                    _mainForm.Close();
                    _mainForm.Dispose();
                    _mainForm = null;
                }

            }
            */
        }


        private void ProcesKsieguj()
        {
            try
            {
                res = false;
                tekstLog = "";

                AttachThreadToClarion();

                string pluginWersja = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                Loguj("Wersja pluginu: " + pluginWersja);

                Plugin1.Konfiguracja.WczytajKonfiguracje(KonfiguracjaGlobalna);
                Plugin1.Konfiguracja.WczytajKonfiguracje(KonfiguracjaLokalna);

                int bladapi = 0;

                if (Plugin1.Konfiguracja._konfiguracje == null)
                    throw new Exception("Nie wczytano konfiguracji.");
                if (Plugin1.Konfiguracja.ListaRejestrow == null)
                    throw new Exception("Nie wczytano konfiguracji globalnej.");
                if (Plugin1.Konfiguracja.fwczytanokonfiguracjelokalna == false)
                    throw new Exception("Nie wczytano konfiguracji lokalnej.");

                polaczenie = new PolaczenieSql(Ingenes.OR.SQLGen.Connection.ConnectionString);
                if (polaczenie.SprawddzPolaczenie())
                { Loguj("Polaczono do sql ied"); }
                else
                { Loguj("Nie polaczono do sql ied"); return; }
                Loguj(2);
                DataTable dt = Ingenes.OR.SQLGen.Select("SELECT * FROM ingenes.DokumentyPrzychodzace LEFT JOIN ingenes.KontaKontrahentow ON Kk_ID=Dp_KontrahentID LEFT JOIN ingenes.RejestryPrzychodzace ON Rdp_ID=Dp_RdpID WHERE Dp_ID = " + idtag).ToTable(); ;
                if (dt.Rows.Count == 1) Loguj("Pobrano dokument. (Dp_ID = " + idtag + ")");

                #region WYBOR KONFIGURACJI REJESTRU

                KonfiguracjaRejestrIED = null;
                foreach (Rejestr r in Plugin1.Konfiguracja.ListaRejestrow)
                {
                    if (r.Nazwa == dt.Rows[0]["Rdp_Nazwa"].ToString())
                        KonfiguracjaRejestrIED = r;
                }
                if (KonfiguracjaRejestrIED == null)
                {
                    Loguj("Nieskonfigurowany rejestr.");
                    //continue;
                    res = false;
                    return;
                }

                #endregion

                if (!dt.Rows[0].IsNull("Dp_Korekta"))
                {
                    if (dt.Rows[0]["Dp_Korekta"].ToString().ToUpper() == "TRUE")
                    {
                        res = true;
                        Loguj("Dokument typu korekta!");
                        if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna))
                        {
                            if (dt.Rows[0].IsNull(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna))
                            {
                            //   res = false;
                               Loguj(String.Format("Brak {0}", KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna));
                            }
                            else
                            {
                                Loguj(String.Format("{0}: {1}", KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna, (dt.Rows[0][KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna]).ToString()));
                            }
                        }
                        return;
                    }
                }
                if (KonfiguracjaRejestrIED.TypDokumentuXl != "AFZ" && KonfiguracjaRejestrIED.TypDokumentuXl != "FZ")
                {
                    Loguj("Nieznany typ dokumentu XL.");
                    res = false;
                    return;
                }

                Loguj(5);

                //try
                //{
                //    //if (String.IsNullOrEmpty(polaczenie.PodajDateObowiazkuPodatkowego(idtag)))
                //    //    throw new Exception();
                //    //DateTime dop = Convert.ToDateTime(polaczenie.PodajDateObowiazkuPodatkowego(idtag));
                //}
                //catch
                //{
                //    //Loguj("Na dokumencie nie podano daty oboowiązku podatkowego.");
                //    //if (XtraMessageBox.Show("Nie podano daty powstania obowiązku podatkowego! Kontynuować ?", "Pytanie:", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                //    //{
                //    //    Loguj("Wybrano nie kontynuuj. Błąd.");
                //    //    Loguj(6);
                //    //    res = false;
                //    //    return;
                //    //}
                //    //Loguj("Wybrano kontynuowanie bez daty powstania obowiązku podatkowego.");
                //}

                int nrblenduxl = 0;


                Xl = new XL();
                try
                {
                    Loguj("Logowanie do XL");
                    string baza = Plugin1.Konfiguracja.baza;
                    string opeIdent = Plugin1.Konfiguracja.opeIdent;
                    string opeHaslo = Plugin1.Konfiguracja.opeHaslo;
                    string serwerKlucza = Plugin1.Konfiguracja.serwerKlucza;
                    string programid = Plugin1.Konfiguracja.programID;
                    Xl.Login(programid, baza, opeIdent, opeHaslo, serwerKlucza, 1);
                    Loguj("Zalogowano do XL Nr. sesji: " + Xl.IdSesji);
                    Loguj(10);
                }
                catch (Exception e)
                {
                    Loguj("[!] Błąd podczas logowania do XL. \r\n     " + e.Message);
                    try
                    {
                        string baza = Plugin1.Konfiguracja.baza;
                        string opeIdent = Plugin1.Konfiguracja.opeIdent;
                        string opeHaslo = Plugin1.Konfiguracja.opeHaslo;
                        string serwerKlucza = Plugin1.Konfiguracja.serwerKlucza;
                        string programid = Plugin1.Konfiguracja.programID;
                        Xl.Login(programid, baza, opeIdent, opeHaslo, serwerKlucza, 0);
                        Loguj("Zalogowano do XL Nr. sesji: " + Xl.IdSesji);
                        Loguj(10);
                    }
                    catch (Exception e1)
                    {
                        throw new Exception("Niezalogowano do xl. Koniec. Blad: " + e1.Message);
                    }
                }


                if (!String.IsNullOrEmpty(Plugin1.Konfiguracja.KontekstOperatoraId))
                {
                    cdn_api.XLZmianaKontekstuOperatora_20163 zmkontekstu = new XLZmianaKontekstuOperatora_20163();
                    zmkontekstu.Wersja = 20163;
                    zmkontekstu.Id = Convert.ToInt32(Plugin1.Konfiguracja.KontekstOperatoraId);
                    bladapi = cdn_api.cdn_api.XLZmianaKontekstuOperatora(zmkontekstu);
                    if (bladapi != 0)
                    {
                        Loguj("Bład podczas zmiany kontekstu operatora: " + bladapi.ToString() + ".");
                    }
                }



                cdn_api.XLPolaczenieInfo_20163 polaczenieinfo = new XLPolaczenieInfo_20163();
                polaczenieinfo.Wersja = 20163;
                cdn_api.cdn_api.XLPolaczenie(polaczenieinfo);


                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
                string[] conparties = polaczenieinfo.ConnectString.Split(';');
                foreach (string s in conparties)
                {
                    if (s.Length <= 1) continue;
                    string[] dualeqspart = s.Split('=');
                    string val = s.Remove(0, dualeqspart[0].Length + 1);
                    if (dualeqspart[0] == "SERVER")
                        sb.DataSource = val;
                    if (dualeqspart[0] == "DATABASE")
                        sb.InitialCatalog = val;
                    if (dualeqspart[0] == "UID")
                        sb.UserID = val;
                    if (dualeqspart[0] == "PWD")
                        sb.Password = val;
                }
                string constring = sb.ConnectionString;
                polaczenie2 = new PolaczenieSql(constring);
                if (polaczenie2.SprawddzPolaczenie())
                { Loguj("Polaczono do sql xl"); }
                else
                { Loguj("Nie polaczono do sql xl"); return; }
                Loguj(20);



                if (dt.Rows.Count > 1)
                {
                    throw new Exception("Blad. Zwrocono wiecej niż jeden dokument.");
                }
                int l = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if (++l > 1) break;
                    //-----

                    #region AFZ
                    if (KonfiguracjaRejestrIED.TypDokumentuXl == "AFZ")
                    {
                        Loguj("Typ dokumentu Xl: AFZ");


                        //    ----------     ----------     ----------     ----------
                        #region Wystawianie dokumentu FS + VAT
                        Loguj("Generowanie dokumentu");
                        dokumentinfo = new XLDokumentNagInfo_20163();
                        dokumentinfo.Typ = 1; //3 lub 2033              - Faktura sprzedaży
                        //1 lub 1521              - Faktura zakupu
                        //dokument.Akronim             
                        int kntnumer = 0;
                        Int32.TryParse(dr["Kk_Reference"].ToString(), out kntnumer);
                        if (kntnumer == 0)
                        {
                            Loguj("Nieprawidłowy numer Kk_Reference (kontrahenta).");
                            break;
                        }
                        dokumentinfo.KntNumer = kntnumer;
                        dokumentinfo.KntTyp = 32;

                        dokumentinfo.Opis = String.IsNullOrEmpty(KonfiguracjaRejestrIED.Opis_kolumna) ? "" : (dr.IsNull(KonfiguracjaRejestrIED.Opis_kolumna) ? "" : dr[KonfiguracjaRejestrIED.Opis_kolumna].ToString());
                        dokumentinfo.Forma = Convert.ToInt32(KonfiguracjaRejestrIED.FormaPlatnosci); //10 - Gotówka 20 - Przelew 30 - Kredyt 40 - Czek 50 - Karta 60 - Inne Wartości pomiedzy pełnymi "dziesiątkami" zarezerwowane na definiowane przez użytkowni                   
                        try
                        {
                            if (String.IsNullOrEmpty(KonfiguracjaRejestrIED.FormaPlatnosci_kolumna))
                            {
                                Loguj("Forma platnosci puste FormaPlatnosci_kolumna");
                            }
                            else
                            {
                                int formaplaatnosci = 0;
                                if (String.IsNullOrEmpty(KonfiguracjaRejestrIED.MapowanieFormaPlatnosci))
                                {
                                    Loguj("Forma platnosci brak mapowania");
                                    if (dr.IsNull(KonfiguracjaRejestrIED.FormaPlatnosci_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString()))
                                        if (Int32.TryParse(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString(), out formaplaatnosci))
                                        {
                                            dokumentinfo.Forma = formaplaatnosci;
                                        }
                                }
                                else
                                {
                                    if (dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna] != null)
                                    {
                                        Loguj("Forma platnosci zawartosc: " + dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString() + " Mapowanie: " + KonfiguracjaRejestrIED.MapowanieFormaPlatnosci);
                                        string fp = Other.WyszukajMapowanie(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString(), KonfiguracjaRejestrIED.MapowanieFormaPlatnosci);
                                        Loguj("Forma platnosci wartosc slownik: [" + fp + "]");
                                        if (Int32.TryParse(fp, out formaplaatnosci))
                                        {
                                            dokumentinfo.Forma = formaplaatnosci;
                                            Loguj("Forma platnosci ustawiono");
                                        }
                                    }
                                    else
                                        Loguj("Forma platnosci brak kolumny: " + KonfiguracjaRejestrIED.FormaPlatnosci_kolumna);

                                }
                            }
                        }
                        catch (Exception efpex)
                        {
                            Loguj("Forma platnosci blad: " + efpex.Message);
                        }
                        dokumentinfo.DokumentObcy = dr["Dp_NumerDokumentu"].ToString();

                        dokumentinfo.Termin = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Termin_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.Termin_kolumna].ToString()));
                        dokumentinfo.Avista = 1;
                        if (dr.IsNull(KonfiguracjaRejestrIED.Data_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()))
                            dokumentinfo.Data = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()));
                        if (dr.IsNull(KonfiguracjaRejestrIED.DataSpr_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()))
                            dokumentinfo.DataSpr = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()));
                        if (dr.IsNull(KonfiguracjaRejestrIED.DataVat_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()))
                            dokumentinfo.DataVat = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()));

                        dokumentinfo.Seria = KonfiguracjaRejestrIED.Seria;
                        dokumentinfo.Rejestr = KonfiguracjaRejestrIED.RejestrXL;
                        Log.Dodaj("Rejestr Kolomna: " + KonfiguracjaRejestrIED.RejestrXL_kolumna);
                        if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.RejestrXL_kolumna))
                        {
                            Log.Dodaj("RK");
                            if (!dr.IsNull(KonfiguracjaRejestrIED.RejestrXL_kolumna))
                            {
                                string rejestr = dr[KonfiguracjaRejestrIED.RejestrXL_kolumna].ToString();
                                Log.Dodaj("Rejestr Kolomna wartosc: " + rejestr);
                                dokumentinfo.Rejestr = rejestr;
                            }
                        }
                        dokumentinfo.Tryb = 2;//1-Interaktywny 2-Wsadowy 3-domyslny
                        dokumentinfo.Waluta = String.IsNullOrEmpty(dr["Dp_Waluta"].ToString()) ? "" : dr["Dp_Waluta"].ToString();

                        #region kurs
                        string kurslog = "";
                        try
                        {
                            if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.Kurs_kolumna))
                            {
                                kurslog += "Konf. kol.: " + KonfiguracjaRejestrIED.Kurs_kolumna;
                                if (dr.Table.Columns.Contains(KonfiguracjaRejestrIED.Kurs_kolumna))
                                {
                                    kurslog += " jest";
                                    string kursstring = ((dr[KonfiguracjaRejestrIED.Kurs_kolumna]) ?? "").ToString().Replace('.', ',');
                                    kurslog += " [" + kursstring + "]";
                                    double kursd = 0;
                                    if (double.TryParse(kursstring, out kursd))
                                    {
                                        kurslog += " vok";
                                        double kurs = kursd * 100;
                                        dokumentinfo.KursL = Convert.ToInt32(kurs);
                                        dokumentinfo.KursM = 100;
                                        Loguj("Kurs: " + dokumentinfo.KursL + " / " + dokumentinfo.KursM);
                                    }
                                }
                            }
                        }
                        catch (Exception kursex)
                        {
                            Loguj("Błąd podczas wyiczania kursu. " + kurslog + " : " + kursex.Message);
                            return;
                        }
                        #endregion

                        if (!String.IsNullOrEmpty(dr["Dp_DataWystawienia"].ToString()))
                            dokumentinfo.Rok = Convert.ToDateTime(dr["Dp_DataWystawienia"].ToString()).Year;

                        if (Convert.ToBoolean(dr["Dp_Korekta"]) == true)
                        {
                            dokumentinfo.Korekta = 1;
                            dokumentinfo.Typ = 1529;
                        }
                        dok = Xl.WystawDokument(dokumentinfo, ref nrblenduxl);
                        Loguj(50);

                        //   foreach (VatInfo tvi in _dokument.ListaVat)


                        //    ----------     ----------     ----------     ----------
                        #region V A T
                        bool fuzyjtabelivat = !String.IsNullOrEmpty(KonfiguracjaRejestrIED.TabelaVat_kolumna);
                        bool fprostyvat = true;
                        if (!fuzyjtabelivat)
                        {
                            fprostyvat = true;
                        }
                        else
                        {
                            #region Tabela VAT
                            Loguj(String.Format("Dodawanie vat (Tabela VAT pole: {0})", KonfiguracjaRejestrIED.TabelaVat_kolumna));
                            bool ftvatok = true;
                            if (!dr.Table.Columns.Contains(KonfiguracjaRejestrIED.TabelaVat_kolumna))
                            {
                                ftvatok = false;
                                Loguj("Brak pola uzytkownika z tabela vat!");
                            }
                            if (dr.IsNull(KonfiguracjaRejestrIED.TabelaVat_kolumna) || String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.TabelaVat_kolumna].ToString()))
                            {
                                ftvatok = false;
                                Loguj("Puste pole uzytkownika z tabela vat!");
                            }
                            if (ftvatok)
                            {
                                fprostyvat = false;
                                string _xml = dr[KonfiguracjaRejestrIED.TabelaVat_kolumna].ToString();
                                tabela tab = new tabela();
                                int nrwierszavat = 0;
                                try
                                {
                                    XmlSerializer des = new XmlSerializer(typeof(tabela));
                                    TextReader tr = new StringReader(_xml);
                                    tab = (tabela)des.Deserialize(tr);
                                    tr.Close();
                                }
                                catch (Exception tvsex)
                                {
                                    ftvatok = false;
                                    Loguj(String.Format("Błąd deserializacji tabeli vat: {0}", tvsex.Message));
                                }
                                if (tab == null || tab.wiersze == null) { ftvatok = false; Loguj("Niepoprawny xml tbela vat"); }
                                if (tab.wiersze.ListaWierszy == null || tab.wiersze.ListaWierszy.Length == 0) { ftvatok = false; Loguj("Brak zdefiniowanych stawek vat"); }
                                if (ftvatok)
                                {
                                    Loguj("Dodawanie vat");
                                    foreach (Wiersz wierzVat in tab.wiersze.ListaWierszy)
                                    {
                                        nrwierszavat++;
                                        XLVatInfo_20163 vatinfo = new XLVatInfo_20163();
                                        int stawkavattabela = 0;
                                        if (Int32.TryParse(wierzVat.stawka, out stawkavattabela))
                                        {
                                            vatinfo.StawkaPod = stawkavattabela * 100; //Convert.ToInt32(tvi.StawkaPod ?? 0) * 100;
                                        }
                                        vatinfo.FlagaVat = 1;//1-Podatek
                                        if (wierzVat.stawka == "NP")
                                            vatinfo.FlagaVat = 2; //2-nie podlega
                                        if (wierzVat.stawka == "ZW")
                                            vatinfo.FlagaVat = 0; //0-zwolniony
                                        vatinfo.Wartosc = wierzVat.netto.ToString().Replace(',', '.');//tvi.KwotaNetto.ToString();//"100,02";//kwota netto
                                        vatinfo.Flagi = 0;
                                        vatinfo.FlagaNB = "N";
                                        vatinfo.Rozliczac = 1;
                                        if (wierzVat.vat != null)
                                            vatinfo.WartoscVat = wierzVat.vat.ToString().Replace(',', '.');
                                        if (!dr.IsNull(KonfiguracjaRejestrIED.DataVat_kolumna))
                                        {
                                            DateTime dataObowiazkuPodatkowego = Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataVat_kolumna]);
                                            vatinfo.DeklRok = dataObowiazkuPodatkowego.Year;
                                            vatinfo.DeklMiesiac = dataObowiazkuPodatkowego.Month;
                                        }
                                        //else
                                        // throw new Exception("Data obowiązku podatkowego jest pusta.");

                                        Xl.DodajVAT(dok, vatinfo);
                                        Loguj(String.Format("Dodano stawke vat {0}", wierzVat.netto));
                                        Loguj(Convert.ToInt32(50 + (((double)20 / (double)tab.wiersze.ListaWierszy.Length) * nrwierszavat)));
                                    }
                                }
                                Loguj(70);
                            }
                            #endregion
                        }

                        if (fprostyvat)
                        {
                            #region Prosty Vat
                            Loguj("Dodawanie vat");
                            XLVatInfo_20163 vatinfo = new XLVatInfo_20163();
                            int stawkavattabela = 0;
                            if (Int32.TryParse(KonfiguracjaRejestrIED.StawkaVAT, out stawkavattabela))
                            {
                                vatinfo.StawkaPod = stawkavattabela * 100; //Convert.ToInt32(tvi.StawkaPod ?? 0) * 100;
                            }
                            vatinfo.FlagaVat = 1;//1-Podatek
                            if (KonfiguracjaRejestrIED.StawkaVAT == "NP")
                                vatinfo.FlagaVat = 2; //2-nie podlega
                            if (KonfiguracjaRejestrIED.StawkaVAT == "ZW")
                                vatinfo.FlagaVat = 0; //0-zwolniony
                            vatinfo.Wartosc = dr["Dp_KwotaNetto"].ToString();//tvi.KwotaNetto.ToString();//"100,02";//kwota netto
                            //  vatinfo.Zrodlowa = 2200;
                            vatinfo.Flagi = 0;
                            vatinfo.FlagaNB = "N";

                            //    vatinfo.WartoscVat = tvi.KwotaVat.ToString();//(((vatinfo.StawkaPod / 100) * Convert.ToDouble(vatinfo.Wartosc)) / 100).ToString();//kwota VAT 
                            vatinfo.Rozliczac = 1;
                            if (!dr.IsNull(KonfiguracjaRejestrIED.DataVat_kolumna))
                            {
                                DateTime dataObowiazkuPodatkowego = Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataVat_kolumna]);
                                vatinfo.DeklRok = dataObowiazkuPodatkowego.Year;
                                vatinfo.DeklMiesiac = dataObowiazkuPodatkowego.Month;
                            }
                            //else
                            //    throw new Exception("Data obowiązku podatkowego jest pusta.");
                            //     vatinfo.DeklMiesiac = _dokument.DataWystawienia.Value.Month;
                            //     vatinfo.DeklRok = _dokument.DataWystawienia.Value.Year;


                            //       Loguj("Numer dokumentu: " + _dokument.NumerDokumentuObcego);
                            //Loguj("dok: " + dok.ToString());
                            //Loguj("vatinfo: " + vatinfo.ToString());
                            Xl.DodajVAT(dok, vatinfo);
                            Loguj(70);
                            #endregion
                        }
                        #endregion
                        //    ----------     ----------     ----------     ----------

                        Xl.ZamknijDokument(dok);
                        Loguj(75);
                        Loguj("Dokument wygenerowany");


                        //    ----------     ----------     ----------     ----------


                        #endregion
                        //    ----------     ----------     ----------     ----------
                        res = true;


                        //    ----------     ----------     ----------     ----------
                        #region Opis analityczny
                        if (String.IsNullOrEmpty(KonfiguracjaRejestrIED.OpisAnalityczny))
                            KonfiguracjaRejestrIED.OpisAnalityczny = "TAK";
                        if (new List<String> { "TAK", "PROBA" }.Contains(KonfiguracjaRejestrIED.OpisAnalityczny.ToUpper()))
                        {
                            Loguj("Dodawanie opisu analitycznego");
                            int opisid = 0;
                            try
                            {
                                cdn_api.XLNowyOpisInfo_20163 nowyopisinfo = new XLNowyOpisInfo_20163();
                                nowyopisinfo.Wersja = 20163;
                                nowyopisinfo.GIDFirma = dokumentinfo.GIDFirma;
                                nowyopisinfo.GIDTyp = dokumentinfo.GIDTyp;
                                nowyopisinfo.GIDNumer = dokumentinfo.GIDNumer;
                                nowyopisinfo.GIDLp = dokumentinfo.GIDLp;
                                nowyopisinfo.Tryb = 0;//?
                                nowyopisinfo.Tresc = "";
                                bladapi = cdn_api.cdn_api.XLNowyOpis(Xl.IdSesji, ref opisid, nowyopisinfo);
                                if (bladapi != 0) { Loguj("Funkcja: XLNowyOpis. Blad api: " + bladapi); res = false; throw new Exception(); }

                                DataTable dtlk = polaczenie.PodajLinieKosztowe(idtag);
                                foreach (DataRow drlk in dtlk.Rows)
                                {
                                    cdn_api.XLDodajLinieOpisuInfo_20163 dodajlinieopisuinfo = new XLDodajLinieOpisuInfo_20163();
                                    dodajlinieopisuinfo.Wersja = 20163;
                                    dodajlinieopisuinfo.Tryb = 0;//?
                                    dodajlinieopisuinfo.Lp = 0;
                                    //dodajlinieopisuinfo.Kierunek = -1; //1 - przychód, -1 - rozchód
                                    dodajlinieopisuinfo.Wartosc = drlk["dpk_kwota"].ToString();//
                                    dodajlinieopisuinfo.Procent = drlk["dpk_procent"].ToString();//
                                    bladapi = cdn_api.cdn_api.XLDodajLinieOpisu(opisid, dodajlinieopisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajLinieOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }

                                    DataTable dtlw = null;
                                    cdn_api.XLDodajWymiarOpisuInfo_20163 dodajwymiaropisuinfo = null;

                                    WykonajMetodeDlaWymiaru(1, opisid, drlk);
                                    WykonajMetodeDlaWymiaru(2, opisid, drlk);
                                    WykonajMetodeDlaWymiaru(3, opisid, drlk);
                                    WykonajMetodeDlaWymiaru(4, opisid, drlk);
                                    WykonajMetodeDlaWymiaru(5, opisid, drlk);
                                    #region stare
                                    /*
                                if (!drlk.IsNull("dpk_Wymiar1_ID"))
                                {
                                    dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_22();
                                    dodajwymiaropisuinfo.Wersja = 22;
                                    dodajwymiaropisuinfo.Tryb = 0;//?
                                    int typ = SprawdzTypWymiaruAnalitycznego(drlk["Wymiar1_Nazwa"].ToString());
                                    dodajwymiaropisuinfo.Typ = typ == 4 ? 0 : typ;
                                    dtlw = polaczenie2.SprawdzIdWymiaru(drlk["dpk_Wymiar1_Kod"].ToString(), typ);
                                    if (dtlw.Rows.Count == 0)
                                        throw new Exception("Nie znaleziono wartości wymiaru: " + drlk["dpk_Wymiar1_Kod"].ToString() + " Typ: " + typ);
                                    if (dtlw.Rows.Count > 1)
                                        throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + drlk["dpk_Wymiar1_Kod"].ToString());
                                    dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                                    //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar1_Kod"].ToString();
                                    //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                                    bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisid, dodajwymiaropisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }

                                if (!drlk.IsNull("dpk_Wymiar2_ID"))
                                {
                                    dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_22();
                                    dodajwymiaropisuinfo.Wersja = 22;
                                    dodajwymiaropisuinfo.Tryb = 0;//?
                                    int typ = SprawdzTypWymiaruAnalitycznego(drlk["Wymiar2_Nazwa"].ToString());
                                    dodajwymiaropisuinfo.Typ = typ == 4 ? 0 : typ;
                                    dtlw = polaczenie2.SprawdzIdWymiaru(drlk["dpk_Wymiar2_Kod"].ToString(), typ);
                                    if (dtlw.Rows.Count == 0)
                                        throw new Exception("Nie znaleziono wartości wymiaru: " + drlk["dpk_Wymiar2_Kod"].ToString() + " Typ: " + typ);
                                    if (dtlw.Rows.Count > 1)
                                        throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + drlk["dpk_Wymiar2_Kod"].ToString());
                                    dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                                    //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar2_Kod"].ToString();
                                    //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                                    bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisid, dodajwymiaropisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }

                                if (!drlk.IsNull("dpk_Wymiar3_ID"))
                                {
                                    dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_22();
                                    dodajwymiaropisuinfo.Wersja = 22;
                                    dodajwymiaropisuinfo.Tryb = 0;//?
                                    int typ = SprawdzTypWymiaruAnalitycznego(drlk["Wymiar3_Nazwa"].ToString());
                                    dodajwymiaropisuinfo.Typ = typ == 4 ? 0 : typ;
                                    dtlw = polaczenie2.SprawdzIdWymiaru(drlk["dpk_Wymiar3_Kod"].ToString(), typ);
                                    if (dtlw.Rows.Count == 0)
                                        throw new Exception("Nie znaleziono wartości wymiaru: " + drlk["dpk_Wymiar3_Kod"].ToString() + " Typ: " + typ);
                                    if (dtlw.Rows.Count > 1)
                                        throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + drlk["dpk_Wymiar3_Kod"].ToString());
                                    dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                                    //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar3_Kod"].ToString();
                                    //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                                    bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisid, dodajwymiaropisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }

                                if (!drlk.IsNull("dpk_Wymiar4_ID"))
                                {
                                    dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_22();
                                    dodajwymiaropisuinfo.Wersja = 22;
                                    dodajwymiaropisuinfo.Tryb = 0;//?
                                    int typ = SprawdzTypWymiaruAnalitycznego(drlk["Wymiar4_Nazwa"].ToString());
                                    dodajwymiaropisuinfo.Typ = typ == 4 ? 0 : typ;
                                    dtlw = polaczenie2.SprawdzIdWymiaru(drlk["dpk_Wymiar4_Kod"].ToString(), typ);
                                    if (dtlw.Rows.Count == 0)
                                        throw new Exception("Nie znaleziono wartości wymiaru: " + drlk["dpk_Wymiar4_Kod"].ToString() + " Typ: " + typ);
                                    if (dtlw.Rows.Count > 1)
                                        throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + drlk["dpk_Wymiar4_Kod"].ToString());
                                    dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                                    //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar4_Kod"].ToString();
                                    //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                                    bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisid, dodajwymiaropisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }

                                if (!drlk.IsNull("dpk_Wymiar5_ID"))
                                {
                                    dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_22();
                                    dodajwymiaropisuinfo.Wersja = 22;
                                    dodajwymiaropisuinfo.Tryb = 0;//?
                                    int typ = SprawdzTypWymiaruAnalitycznego(drlk["Wymiar5_Nazwa"].ToString());
                                    dodajwymiaropisuinfo.Typ = typ == 4 ? 0 : typ;
                                    dtlw = polaczenie2.SprawdzIdWymiaru(drlk["dpk_Wymiar5_Kod"].ToString(), typ);
                                    if (dtlw.Rows.Count == 0)
                                        throw new Exception("Nie znaleziono wartości wymiaru: " + drlk["dpk_Wymiar5_Kod"].ToString() + " Typ: " + typ);
                                    if (dtlw.Rows.Count > 1)
                                        throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + drlk["dpk_Wymiar5_Kod"].ToString());
                                    dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                                    //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar5_Kod"].ToString();
                                    //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                                    bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisid, dodajwymiaropisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }
                                */
                                    #endregion
                                    cdn_api.XLZamknijLinieOpisuInfo_20163 zamknijlinieopisuinfo = new XLZamknijLinieOpisuInfo_20163();
                                    zamknijlinieopisuinfo.Wersja = 20163;
                                    bladapi = cdn_api.cdn_api.XLZamknijLinieOpisu(opisid, zamknijlinieopisuinfo);
                                    if (bladapi != 0) { Loguj("Funkcja: XLZamknijLinieOpisu. Blad api: " + bladapi); res = false; throw new Exception(); }
                                }
                                cdn_api.XLZamknijOpisInfo_20163 zamknijopisinfo = new XLZamknijOpisInfo_20163();
                                zamknijopisinfo.Wersja = 20163;
                                bladapi = cdn_api.cdn_api.XLZamknijOpis(opisid, zamknijopisinfo);
                                if (bladapi != 0) { Loguj("Funkcja: XLZamknijOpis. Blad api: " + bladapi); res = false; throw new Exception(); }

                            }
                            catch (Exception exoa)
                            {
                                if (KonfiguracjaRejestrIED.OpisAnalityczny.ToUpper() == "TAK")
                                    res = false;
                                Loguj("Bład podczas tworzenia opisu analitycznego. " + exoa.Message);
                                try
                                {
                                    if (opisid != 0)
                                    {
                                        Loguj("Zamykanie opisu.");
                                        cdn_api.XLZamknijOpisInfo_20163 zamknijopisinfo = new XLZamknijOpisInfo_20163();
                                        zamknijopisinfo.Wersja = 20163;
                                        bladapi = cdn_api.cdn_api.XLZamknijOpis(opisid, zamknijopisinfo);
                                        if (bladapi != 0)
                                            throw new Exception("Funkcja: XLZamknijOpis. Blad api: " + bladapi);
                                    }
                                }
                                catch (Exception exoaz)
                                {
                                    Loguj("Blad. Blad podczs zamykania opisu po błędzie. " + exoaz.Message);
                                }
                            }
                        }
                        Loguj(80);
                        #endregion
                        //    ----------     ----------     ----------     ----------



                        //    ----------     ----------     ----------     ----------
                        #region Dodawanie atrybutu
                        string atrybutwartosc = "";
                        atrybutwartosc += "ied://";
                        atrybutwartosc += Bll.BazaDanych;
                        atrybutwartosc += "/dp/";
                        atrybutwartosc += polaczenie.PodajGuidDokumentuPrzychodzacego(idtag);
                        atrybutwartosc += "/[Edycja]";
                        //
                        cdn_api.XLAtrybutInfo_20163 atrinfo = new XLAtrybutInfo_20163();
                        atrinfo.Wersja = 20163;
                        atrinfo.GIDFirma = dokumentinfo.GIDFirma;
                        atrinfo.GIDTyp = dokumentinfo.GIDTyp;
                        atrinfo.GIDNumer = dokumentinfo.GIDNumer;
                        atrinfo.GIDLp = dokumentinfo.GIDLp;
                        atrinfo.Klasa = "IED_EDOK2XL";
                        atrinfo.Wartosc = atrybutwartosc;
                        bladapi = cdn_api.cdn_api.XLDodajAtrybut(Xl.IdSesji, atrinfo);
                        if (bladapi != 0)
                        {
                            #region blad opis
                            string bladopis = "";
                            if (bladapi == -1)
                                bladopis = " brak sesji";
                            if (bladapi == 2)
                                bladopis = " błąd przy zakładaniu logout";
                            if (bladapi == 3)
                                bladopis = "nie znaleziono obiektu";
                            if (bladapi == 4)
                                bladopis = "nie znalezniono klasy atrybutu";
                            if (bladapi == 5)
                                bladopis = "klasa nieprzypisana do definicji obiektu";
                            if (bladapi == 6)
                                bladopis = "atrybut juz istnieje w kolejce";
                            if (bladapi == 7)
                                bladopis = "błąd ADO Connection";
                            if (bladapi == 8)
                                bladopis = "błąd ADO";
                            if (bladapi == 9)
                                bladopis = "brak zdefiniowanego obiektu";
                            #endregion
                            Loguj("Funkcja: XLDodajAtrybut. Blad api: " + bladapi + " " + bladopis);
                        }
                        else Loguj("Dodano atrybut EDOK: " + atrybutwartosc);

                        Loguj(90);
                        #endregion
                        //    ----------     ----------     ----------     ----------



                        //    ----------     ----------     ----------     ----------
                        #region Zapis numeru z XL w bazie EDOK, podnoszenie dokumentu w XL
                        cdn_api.XLNumerDokumentuInfo_20163 numerdok = new XLNumerDokumentuInfo_20163();
                        numerdok.Wersja = 20163;
                        numerdok.GIDTyp = dokumentinfo.GIDTyp;
                        numerdok.GIDNumer = dokumentinfo.GIDNumer;
                        numerdok.GIDLp = dokumentinfo.GIDLp;
                        numerdok.GIDFirma = dokumentinfo.GIDFirma;
                        cdn_api.cdn_api.XLPobierzNumerDokumentu(numerdok);

                        Loguj("Zapisywanie numeru dokumentu w bazie edok. (" + idtag + " : " + numerdok.NumerDokumentu + ")");
                        //polaczenie.UpdateXLNumer(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna, numerdok.NumerDokumentu, idtag);

                        string kolumna = KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna;
                        string wartosc =numerdok.NumerDokumentu;
                       
                        UpdejtujNumerDokumentu(kolumna, wartosc);


                        if (Plugin1.Konfiguracja.pokazdokument)
                        {
                            cdn_api.XLGIDGrupaInfo_20163 grupinfo = new XLGIDGrupaInfo_20163();
                            grupinfo.Wersja = 20163;
                            grupinfo.GIDFirma = dokumentinfo.GIDFirma;
                            grupinfo.GIDTyp = dokumentinfo.GIDTyp;
                            grupinfo.GIDNumer = dokumentinfo.GIDNumer;
                            grupinfo.GIDLp = dokumentinfo.GIDLp;
                            bladapi = cdn_api.cdn_api.XLUruchomFormatkeWgGID(grupinfo);
                            if (bladapi != 0)
                            {
                                #region bladopis
                                string bladopis = "";
                                if (bladapi == 2)
                                    bladopis = "ustalono wybór z listy, a brak ustawienia trybu selekcji";
                                if (bladapi == 3)
                                    bladopis = "błąd otwarcia tabeli";
                                if (bladapi == 4)
                                    bladopis = "nie znaleziono rekordu o podanym GIDzie";
                                if (bladapi == 5)
                                    bladopis = "ustalono wybór elementu z listy a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 6)
                                    bladopis = "ustalono wybór grupy z listy, a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 7)
                                    bladopis = "ustalono podgląd elementów listy, a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 8)
                                    bladopis = "nieobsługiwany GIDTyp";
                                #endregion
                                Loguj("Blad podonoszenia dokumentu XL. Blad XLUruchomFormatkeWgGID: " + bladapi.ToString() + " " + bladopis);
                            }
                        }
                        #endregion
                        //    ----------     ----------     ----------     ----------

                    }
                    #endregion

                    #region FZ
                    if (KonfiguracjaRejestrIED.TypDokumentuXl == "FZ")
                    {
                        Loguj("Typ dokumentu Xl: FZ");

                        //    ----------     ----------     ----------     ----------
                        #region Wystawianie dokumentu FS
                        Loguj("Generowanie dokumentu");
                        dokumentinfo = new XLDokumentNagInfo_20163();
                        dokumentinfo.Typ = 1; //3 lub 2033              - Faktura sprzedaży
                        //1 lub 1521              - Faktura zakupu
                        //dokument.Akronim             
                        int kntnumer = 0;
                        Int32.TryParse(dr["Kk_Reference"].ToString(), out kntnumer);
                        if (kntnumer == 0)
                        {
                            Loguj("Nieprawidłowy numer Kk_Reference (kontrahenta).");
                            break;
                        }
                        dokumentinfo.KntNumer = kntnumer;
                        dokumentinfo.KntTyp = 32;

                        dokumentinfo.Opis = String.IsNullOrEmpty(KonfiguracjaRejestrIED.Opis_kolumna) ? "" : (dr.IsNull(KonfiguracjaRejestrIED.Opis_kolumna) ? "" : dr[KonfiguracjaRejestrIED.Opis_kolumna].ToString());
                        dokumentinfo.Forma = Convert.ToInt32(KonfiguracjaRejestrIED.FormaPlatnosci); //10 - Gotówka 20 - Przelew 30 - Kredyt 40 - Czek 50 - Karta 60 - Inne Wartości pomiedzy pełnymi "dziesiątkami" zarezerwowane na definiowane przez użytkowni                   
                        try
                        {
                            if (String.IsNullOrEmpty(KonfiguracjaRejestrIED.FormaPlatnosci_kolumna))
                            {
                                Loguj("Forma platnosci puste FormaPlatnosci_kolumna");
                            }
                            else
                            {
                                int formaplaatnosci = 0;
                                if (String.IsNullOrEmpty(KonfiguracjaRejestrIED.MapowanieFormaPlatnosci))
                                {
                                    Loguj("Forma platnosci brak mapowania");
                                    if (dr.IsNull(KonfiguracjaRejestrIED.FormaPlatnosci_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString()))
                                        if (Int32.TryParse(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString(), out formaplaatnosci))
                                        {
                                            dokumentinfo.Forma = formaplaatnosci;
                                        }
                                }
                                else
                                {
                                    if (dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna] != null)
                                    {
                                        Loguj("Forma platnosci zawartosc: " + dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString() + " Mapowanie: " + KonfiguracjaRejestrIED.MapowanieFormaPlatnosci);
                                        string fp = Other.WyszukajMapowanie(dr[KonfiguracjaRejestrIED.FormaPlatnosci_kolumna].ToString(), KonfiguracjaRejestrIED.MapowanieFormaPlatnosci);
                                        Loguj("Forma platnosci wartosc slownik: [" + fp + "]");
                                        if (Int32.TryParse(fp, out formaplaatnosci))
                                        {
                                            dokumentinfo.Forma = formaplaatnosci;
                                            Loguj("Forma platnosci ustawiono");
                                        }
                                    }
                                    else
                                        Loguj("Forma platnosci brak kolumny: " + KonfiguracjaRejestrIED.FormaPlatnosci_kolumna);

                                }
                            }
                        }
                        catch (Exception efpex)
                        {
                            Loguj("Forma platnosci blad: " + efpex.Message);
                        }
                        dokumentinfo.DokumentObcy = dr["Dp_NumerDokumentu"].ToString();

                        dokumentinfo.Termin = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Termin_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.Termin_kolumna].ToString()));
                        //dokumentinfo.Avista = 0;
                        if (dr.IsNull(KonfiguracjaRejestrIED.Data_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()))
                            dokumentinfo.Data = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.Data_kolumna].ToString()));
                        if (dr.IsNull(KonfiguracjaRejestrIED.DataSpr_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()))
                            dokumentinfo.DataSpr = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString()));
                        if (dr.IsNull(KonfiguracjaRejestrIED.DataVat_kolumna) ? false : !String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()))
                            dokumentinfo.DataVat = String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()) ? 0 : DateToClariion(Convert.ToDateTime(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString()));

                        DateTime DataVat;
                        if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.DataVat_kolumna))
                        {
                            if (DateTime.TryParse(dr[KonfiguracjaRejestrIED.DataVat_kolumna].ToString(), out DataVat))
                            {
                                if (DataVat.Month > 9)
                                    KonfiguracjaRejestrIED.Seria = KonfiguracjaRejestrIED.Seria.Replace("{Miesiac}", DataVat.Month.ToString());
                                else
                                    KonfiguracjaRejestrIED.Seria = KonfiguracjaRejestrIED.Seria.Replace("{Miesiac}", "0" + DataVat.Month.ToString());
                            }
                        }
                        KonfiguracjaRejestrIED.Seria = KonfiguracjaRejestrIED.Seria.Replace("{Miesiac}", "");
                        dokumentinfo.Seria = KonfiguracjaRejestrIED.Seria; Log.Dodaj("dokumentinfo.Seria: " + dokumentinfo.Seria);
                        dokumentinfo.Rejestr = KonfiguracjaRejestrIED.RejestrXL;
                        Log.Dodaj("Rejestr Kolomna: " + KonfiguracjaRejestrIED.RejestrXL_kolumna);
                        if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.RejestrXL_kolumna)) 
                        {
                            Log.Dodaj("RK");
                            if (!dr.IsNull(KonfiguracjaRejestrIED.RejestrXL_kolumna)) 
                            {                               
                                string rejestr = dr[KonfiguracjaRejestrIED.RejestrXL_kolumna].ToString();
                                Log.Dodaj("Rejestr Kolomna wartosc: " + rejestr);
                                dokumentinfo.Rejestr = rejestr;
                            }
                        }
                        dokumentinfo.Tryb = 2;//1-Interaktywny 2-Wsadowy 3-domyslny
                        dokumentinfo.Waluta = String.IsNullOrEmpty(dr["Dp_Waluta"].ToString()) ? "" : dr["Dp_Waluta"].ToString();

                        #region kurs
                        string kurslog = "";
                        try
                        {
                            if (!String.IsNullOrEmpty(KonfiguracjaRejestrIED.Kurs_kolumna))
                            {
                                kurslog += "Konf. kol.: " + KonfiguracjaRejestrIED.Kurs_kolumna;
                                if (dr.Table.Columns.Contains(KonfiguracjaRejestrIED.Kurs_kolumna))
                                {
                                    kurslog += " jest";
                                    string kursstring = ((dr[KonfiguracjaRejestrIED.Kurs_kolumna]) ?? "").ToString().Replace('.', ',');
                                    kurslog += " [" + kursstring + "]";
                                    double kursd = 0;
                                    if (double.TryParse(kursstring, out kursd))
                                    {
                                        kurslog += " vok";
                                        double kurs = kursd * 100;
                                        dokumentinfo.KursL = Convert.ToInt32(kurs);
                                        dokumentinfo.KursM = 100;
                                        Loguj("Kurs: " + dokumentinfo.KursL + " / " + dokumentinfo.KursM);
                                    }
                                }
                            }
                        }
                        catch (Exception kursex)
                        {
                            Loguj("Błąd podczas wyiczania kursu. " + kurslog + " : " + kursex.Message);
                            return;
                        }
                        #endregion

                        if (!String.IsNullOrEmpty(dr["Dp_DataWystawienia"].ToString()))
                            dokumentinfo.Rok = Convert.ToDateTime(dr["Dp_DataWystawienia"].ToString()).Year;
                        //DateTime DataSpr;
                        //if (DateTime.TryParse(dr[KonfiguracjaRejestrIED.DataSpr_kolumna].ToString(), out DataSpr)) 
                        //{
                        //    dokumentinfo.Rok = DataSpr.Year;
                        //    dokumentinfo.Miesiac = DataSpr.Month;
                        //}


                        dok = Xl.WystawDokument(dokumentinfo, ref nrblenduxl);
                        Loguj(50);

                        //   foreach (VatInfo tvi in _dokument.ListaVat)


                        //    ----------     ----------     ----------     ----------
                        #region V A T //
                        //bool fuzyjtabelivat = !String.IsNullOrEmpty(KonfiguracjaRejestrIED.TabelaVat_kolumna);
                        //bool fprostyvat = true;
                        //if (!fuzyjtabelivat)
                        //{
                        //    fprostyvat = true;
                        //}
                        //else
                        //{
                        //    #region Tabela VAT
                        //    Loguj(String.Format("Dodawanie vat (Tabela VAT pole: {0})", KonfiguracjaRejestrIED.TabelaVat_kolumna));
                        //    bool ftvatok = true;
                        //    if (!dr.Table.Columns.Contains(KonfiguracjaRejestrIED.TabelaVat_kolumna))
                        //    {
                        //        ftvatok = false;
                        //        Loguj("Brak pola uzytkownika z tabela vat!");
                        //    }
                        //    if (dr.IsNull(KonfiguracjaRejestrIED.TabelaVat_kolumna) || String.IsNullOrEmpty(dr[KonfiguracjaRejestrIED.TabelaVat_kolumna].ToString()))
                        //    {
                        //        ftvatok = false;
                        //        Loguj("Puste pole uzytkownika z tabela vat!");
                        //    }
                        //    if (ftvatok)
                        //    {
                        //        fprostyvat = false;
                        //        string _xml = dr[KonfiguracjaRejestrIED.TabelaVat_kolumna].ToString();
                        //        tabela tab = new tabela();
                        //        int nrwierszavat = 0;
                        //        try
                        //        {
                        //            XmlSerializer des = new XmlSerializer(typeof(tabela));
                        //            TextReader tr = new StringReader(_xml);
                        //            tab = (tabela)des.Deserialize(tr);
                        //            tr.Close();
                        //        }
                        //        catch (Exception tvsex)
                        //        {
                        //            ftvatok = false;
                        //            Loguj(String.Format("Błąd deserializacji tabeli vat: {0}", tvsex.Message));
                        //        }
                        //        if (tab == null || tab.wiersze == null) { ftvatok = false; Loguj("Niepoprawny xml tbela vat"); }
                        //        if (tab.wiersze.ListaWierszy == null || tab.wiersze.ListaWierszy.Length == 0) { ftvatok = false; Loguj("Brak zdefiniowanych stawek vat"); }
                        //        if (ftvatok)
                        //        {
                        //            Loguj("Dodawanie vat");
                        //            foreach (Wiersz wierzVat in tab.wiersze.ListaWierszy)
                        //            {
                        //                nrwierszavat++;
                        //                XLVatInfo_20141 vatinfo = new XLVatInfo_20141();
                        //                int stawkavattabela = 0;
                        //                if (Int32.TryParse(wierzVat.stawka, out stawkavattabela))
                        //                {
                        //                    vatinfo.StawkaPod = stawkavattabela * 100; //Convert.ToInt32(tvi.StawkaPod ?? 0) * 100;
                        //                }
                        //                vatinfo.FlagaVat = 1;//1-Podatek
                        //                if (wierzVat.stawka == "NP")
                        //                    vatinfo.FlagaVat = 2; //2-nie podlega
                        //                if (wierzVat.stawka == "ZW")
                        //                    vatinfo.FlagaVat = 0; //0-zwolniony
                        //                vatinfo.Wartosc = wierzVat.netto.ToString().Replace(',', '.');//tvi.KwotaNetto.ToString();//"100,02";//kwota netto
                        //                vatinfo.Flagi = 0;
                        //                vatinfo.FlagaNB = "N";
                        //                vatinfo.Rozliczac = 1;
                        //                if (wierzVat.vat != null)
                        //                    vatinfo.WartoscVat = wierzVat.vat.ToString().Replace(',', '.');
                        //                if (!dr.IsNull("Dp_DataObowiazkuPodatkowego"))
                        //                {
                        //                    DateTime dataObowiazkuPodatkowego = Convert.ToDateTime(dr["Dp_DataObowiazkuPodatkowego"]);
                        //                    vatinfo.DeklRok = dataObowiazkuPodatkowego.Year;
                        //                    vatinfo.DeklMiesiac = dataObowiazkuPodatkowego.Month;
                        //                }
                        //                //else
                        //                // throw new Exception("Data obowiązku podatkowego jest pusta.");

                        //                Xl.DodajVAT(dok, vatinfo);
                        //                Loguj(String.Format("Dodano stawke vat {0}", wierzVat.netto));
                        //                Loguj(Convert.ToInt32(50 + (((double)20 / (double)tab.wiersze.ListaWierszy.Length) * nrwierszavat)));
                        //            }
                        //        }
                        //        Loguj(70);
                        //    }
                        //    #endregion
                        //}

                        //if (fprostyvat)
                        //{
                        //    #region Prosty Vat
                        //    Loguj("Dodawanie vat");
                        //    XLVatInfo_20141 vatinfo = new XLVatInfo_20141();
                        //    int stawkavattabela = 0;
                        //    if (Int32.TryParse(KonfiguracjaRejestrIED.StawkaVAT, out stawkavattabela))
                        //    {
                        //        vatinfo.StawkaPod = stawkavattabela * 100; //Convert.ToInt32(tvi.StawkaPod ?? 0) * 100;
                        //    }
                        //    vatinfo.FlagaVat = 1;//1-Podatek
                        //    if (KonfiguracjaRejestrIED.StawkaVAT == "NP")
                        //        vatinfo.FlagaVat = 2; //2-nie podlega
                        //    if (KonfiguracjaRejestrIED.StawkaVAT == "ZW")
                        //        vatinfo.FlagaVat = 0; //0-zwolniony
                        //    vatinfo.Wartosc = dr["Dp_KwotaNetto"].ToString();//tvi.KwotaNetto.ToString();//"100,02";//kwota netto
                        //    //  vatinfo.Zrodlowa = _2014100;
                        //    vatinfo.Flagi = 0;
                        //    vatinfo.FlagaNB = "N";

                        //    //    vatinfo.WartoscVat = tvi.KwotaVat.ToString();//(((vatinfo.StawkaPod / 100) * Convert.ToDouble(vatinfo.Wartosc)) / 100).ToString();//kwota VAT 
                        //    vatinfo.Rozliczac = 1;
                        //    if (!dr.IsNull("Dp_DataObowiazkuPodatkowego"))
                        //    {
                        //        DateTime dataObowiazkuPodatkowego = Convert.ToDateTime(dr["Dp_DataObowiazkuPodatkowego"]);
                        //        vatinfo.DeklRok = dataObowiazkuPodatkowego.Year;
                        //        vatinfo.DeklMiesiac = dataObowiazkuPodatkowego.Month;
                        //    }
                        //    //else
                        //    //    throw new Exception("Data obowiązku podatkowego jest pusta.");
                        //    //     vatinfo.DeklMiesiac = _dokument.DataWystawienia.Value.Month;
                        //    //     vatinfo.DeklRok = _dokument.DataWystawienia.Value.Year;


                        //    //       Loguj("Numer dokumentu: " + _dokument.NumerDokumentuObcego);
                        //    //Loguj("dok: " + dok.ToString());
                        //    //Loguj("vatinfo: " + vatinfo.ToString());
                        //    Xl.DodajVAT(dok, vatinfo);
                        //    Loguj(70);
                        //    #endregion
                        //}
                        #endregion
                        //    ----------     ----------     ----------     ----------


                        //    ----------     ----------     ----------     ----------
                        List<XLDokumentElemInfo_20163> listaPozycjeInfo = new List<XLDokumentElemInfo_20163>();
                        #region P o z y c j e

                        DataTable dtpoz = polaczenie.PodajLinieKosztowe(idtag);//Ingenes.OR.SQLGen.Select("SELECT *,w1.wmr_nazwa as Wymiar1_Nazwa,w2.wmr_nazwa as Wymiar2_Nazwa,w3.wmr_nazwa as Wymiar3_Nazwa,w4.wmr_nazwa as Wymiar4_Nazwa,w5.wmr_nazwa as Wymiar5_Nazwa FROM ingenes.DokumentyPrzychodzaceKoszty LEFT JOIN ingenes.Wymiary w1 ON w1.wmr_id=dpk_Wymiar1_ID LEFT JOIN ingenes.Wymiary w2 ON w2.wmr_id=dpk_Wymiar2_ID LEFT JOIN ingenes.Wymiary w3 ON w3.wmr_id=dpk_Wymiar3_ID LEFT JOIN ingenes.Wymiary w4 ON w4.wmr_id=dpk_Wymiar4_ID LEFT JOIN ingenes.Wymiary w5 ON w5.wmr_id=dpk_Wymiar5_ID WHERE dpk_dpID=" + idtag).ToTable(); ;
                        if (dtpoz.Rows.Count > 0)
                        {
                            foreach (DataRow drpoz in dtpoz.Rows)
                            {
                                string kolumnaOpis = "";
                                if ((drpoz.IsNull("Wymiar1_Nazwa") ? "" : drpoz["Wymiar1_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaOpis_wymiar)
                                    kolumnaOpis = "dpk_Wymiar1_Kod";
                                if ((drpoz.IsNull("Wymiar2_Nazwa") ? "" : drpoz["Wymiar2_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaOpis_wymiar)
                                    kolumnaOpis = "dpk_Wymiar2_Kod";
                                if ((drpoz.IsNull("Wymiar3_Nazwa") ? "" : drpoz["Wymiar3_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaOpis_wymiar)
                                    kolumnaOpis = "dpk_Wymiar3_Kod";
                                if ((drpoz.IsNull("Wymiar4_Nazwa") ? "" : drpoz["Wymiar4_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaOpis_wymiar)
                                    kolumnaOpis = "dpk_Wymiar4_Kod";
                                if ((drpoz.IsNull("Wymiar5_Nazwa") ? "" : drpoz["Wymiar5_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaOpis_wymiar)
                                    kolumnaOpis = "dpk_Wymiar5_Kod";
                                string opis = drpoz.IsNull(kolumnaOpis) ? "" : drpoz[kolumnaOpis].ToString();


                                string kolumnaVat = "";
                                if ((drpoz.IsNull("Wymiar1_Nazwa") ? "" : drpoz["Wymiar1_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaStawkaVAT_wymiar)
                                    kolumnaVat = "dpk_Wymiar1_Kod";
                                if ((drpoz.IsNull("Wymiar2_Nazwa") ? "" : drpoz["Wymiar2_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaStawkaVAT_wymiar)
                                    kolumnaVat = "dpk_Wymiar2_Kod";
                                if ((drpoz.IsNull("Wymiar3_Nazwa") ? "" : drpoz["Wymiar3_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaStawkaVAT_wymiar)
                                    kolumnaVat = "dpk_Wymiar3_Kod";
                                if ((drpoz.IsNull("Wymiar4_Nazwa") ? "" : drpoz["Wymiar4_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaStawkaVAT_wymiar)
                                    kolumnaVat = "dpk_Wymiar4_Kod";
                                if ((drpoz.IsNull("Wymiar5_Nazwa") ? "" : drpoz["Wymiar5_Nazwa"].ToString()) == KonfiguracjaRejestrIED.PozycjaStawkaVAT_wymiar)
                                    kolumnaVat = "dpk_Wymiar5_Kod";
                                string vat = drpoz.IsNull(kolumnaVat) ? "" : drpoz[kolumnaVat].ToString();
                                Log.Dodaj("Linia kosztowa: kolumna opis: " + kolumnaOpis + " opis: " + opis + " kolumna vat: " + kolumnaVat + " vat: " + vat);
                                if (String.IsNullOrEmpty(vat))
                                {
                                    vat = KonfiguracjaRejestrIED.StawkaVAT;
                                }
                                if (listaPozycjeInfo.Find(p => { return p.Opis == opis; }) != null)
                                {
                                    Log.Dodaj("Pominęto linie.");
                                    continue;
                                }
                                int ivat = 0;
                                if (Int32.TryParse(vat, out ivat))
                                {
                                    ivat = ivat * 100;
                                }
                                XLDokumentElemInfo_20163 pozycjainfo = new XLDokumentElemInfo_20163();
                                pozycjainfo.Wersja = 20163;
                                pozycjainfo.Ilosc = "1";
                                pozycjainfo.TowarNazwa = opis;
                                pozycjainfo.PrzeliczL = 1;
                                pozycjainfo.PrzeliczM = 1;
                                pozycjainfo.TowarKod = "A-VISTA";
                                pozycjainfo.JmZ = KonfiguracjaRejestrIED.PozycjaJm;
                                pozycjainfo.StawkaPod = ivat;
                                pozycjainfo.Cena = drpoz["dpk_kwota"].ToString();

                                listaPozycjeInfo.Add(pozycjainfo);
                            }
                            int lpoz = listaPozycjeInfo.Count;
                            foreach (XLDokumentElemInfo_20163 pozycjainfo in listaPozycjeInfo)
                            {
                                try
                                {
                                    Loguj(String.Format("Dodawanie pozycji. Opis: {0} Jm: {1} Vat: {2}", pozycjainfo.Opis, pozycjainfo.JmZ, pozycjainfo.Vat));
                                    bladapi = cdn_api.cdn_api.XLDodajPozycje(dok, pozycjainfo);
                                    if (bladapi != 0)
                                        throw new Exception("XLDodajPozycje Blad api: " + bladapi);
                                    Loguj(60);
                                }
                                catch (Exception pdex)
                                {
                                    Loguj("Błąd podczas dodawania pozycji. " + pdex.Message);
                                }
                            }


                        }
                        else
                        {
                            Loguj("Brak pozycji - lini kosztowcych!");
                            res = false;
                            return;
                        }
                        #endregion
                        //    ----------     ----------     ----------     ----------



                        Xl.ZamknijDokument(dok, 1);
                        Loguj(75);
                        Loguj("Dokument wygenerowany");
                        res = true;

                        //cdn_api.XLGIDGrupaInfo_20141 grupinfo = new XLGIDGrupaInfo_20141();
                        //grupinfo.Wersja = 20141;
                        //grupinfo.GIDFirma = dokumentinfo.GIDFirma;
                        //grupinfo.GIDTyp = dokumentinfo.GIDTyp;
                        //grupinfo.GIDNumer = dokumentinfo.GIDNumer;
                        //grupinfo.GIDLp = dokumentinfo.GIDLp;
                        //bladapi = cdn_api.cdn_api.XLUruchomFormatkeWgGID(grupinfo);
                        //if (bladapi != 0)
                        //{
                        //    #region bladopis
                        //    string bladopis = "";
                        //    if (bladapi == 2)
                        //        bladopis = "ustalono wybór z listy, a brak ustawienia trybu selekcji";
                        //    if (bladapi == 3)
                        //        bladopis = "błąd otwarcia tabeli";
                        //    if (bladapi == 4)
                        //        bladopis = "nie znaleziono rekordu o podanym GIDzie";
                        //    if (bladapi == 5)
                        //        bladopis = "ustalono wybór elementu z listy a brak procedury listy dla takiego typu dokumentu";
                        //    if (bladapi == 6)
                        //        bladopis = "ustalono wybór grupy z listy, a brak procedury listy dla takiego typu dokumentu";
                        //    if (bladapi == 7)
                        //        bladopis = "ustalono podgląd elementów listy, a brak procedury listy dla takiego typu dokumentu";
                        //    if (bladapi == 8)
                        //        bladopis = "nieobsługiwany GIDTyp";
                        //    #endregion
                        //    Loguj("Blad podonoszenia dokumentu XL. Blad XLUruchomFormatkeWgGID: " + bladapi.ToString() + " " + bladopis);
                        //}

                        //    ----------     ----------     ----------     ----------


                        #endregion
                        //    ----------     ----------     ----------     ----------


                        //    ----------     ----------     ----------     ----------
                        #region Dodawanie atrybutu
                        string atrybutwartosc = "";
                        atrybutwartosc += "ied://";
                        atrybutwartosc += Bll.BazaDanych;
                        atrybutwartosc += "/dp/";
                        atrybutwartosc += polaczenie.PodajGuidDokumentuPrzychodzacego(idtag);
                        atrybutwartosc += "/[Edycja]";
                        //
                        cdn_api.XLAtrybutInfo_20163 atrinfo = new XLAtrybutInfo_20163();
                        atrinfo.Wersja = 20163;
                        atrinfo.GIDFirma = dokumentinfo.GIDFirma;
                        atrinfo.GIDTyp = dokumentinfo.GIDTyp;
                        atrinfo.GIDNumer = dokumentinfo.GIDNumer;
                        atrinfo.GIDLp = dokumentinfo.GIDLp;
                        atrinfo.Klasa = "IED_EDOK2XL";
                        atrinfo.Wartosc = atrybutwartosc;
                        bladapi = cdn_api.cdn_api.XLDodajAtrybut(Xl.IdSesji, atrinfo);
                        if (bladapi != 0)
                        {
                            #region blad opis
                            string bladopis = "";
                            if (bladapi == -1)
                                bladopis = " brak sesji";
                            if (bladapi == 2)
                                bladopis = " błąd przy zakładaniu logout";
                            if (bladapi == 3)
                                bladopis = "nie znaleziono obiektu";
                            if (bladapi == 4)
                                bladopis = "nie znalezniono klasy atrybutu";
                            if (bladapi == 5)
                                bladopis = "klasa nieprzypisana do definicji obiektu";
                            if (bladapi == 6)
                                bladopis = "atrybut juz istnieje w kolejce";
                            if (bladapi == 7)
                                bladopis = "błąd ADO Connection";
                            if (bladapi == 8)
                                bladopis = "błąd ADO";
                            if (bladapi == 9)
                                bladopis = "brak zdefiniowanego obiektu";
                            #endregion
                            Loguj("Funkcja: XLDodajAtrybut. Blad api: " + bladapi + " " + bladopis);
                        }
                        else Loguj("Dodano atrybut EDOK: " + atrybutwartosc);

                        Loguj(90);
                        #endregion
                        //    ----------     ----------     ----------     ----------

                        //    ----------     ----------     ----------     ----------
                        #region Zapis numeru z XL w bazie EDOK, podnoszenie dokumentu w XL
                        cdn_api.XLNumerDokumentuInfo_20163 numerdok = new XLNumerDokumentuInfo_20163();
                        numerdok.Wersja = 20163;
                        numerdok.GIDTyp = dokumentinfo.GIDTyp;
                        numerdok.GIDNumer = dokumentinfo.GIDNumer;
                        numerdok.GIDLp = dokumentinfo.GIDLp;
                        numerdok.GIDFirma = dokumentinfo.GIDFirma;
                        cdn_api.cdn_api.XLPobierzNumerDokumentu(numerdok);

                        Loguj("Zapisywanie numeru dokumentu w bazie edok. (" + idtag + " : " + numerdok.NumerDokumentu + ")");
                        //polaczenie.UpdateXLNumer(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna, numerdok.NumerDokumentu, idtag);
                        UpdejtujNumerDokumentu(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna, numerdok.NumerDokumentu);


                        if (Plugin1.Konfiguracja.pokazdokument)
                        {
                            cdn_api.XLGIDGrupaInfo_20163 grupinfo = new XLGIDGrupaInfo_20163();
                            grupinfo.Wersja = 20163;
                            grupinfo.GIDFirma = dokumentinfo.GIDFirma;
                            grupinfo.GIDTyp = dokumentinfo.GIDTyp;
                            grupinfo.GIDNumer = dokumentinfo.GIDNumer;
                            grupinfo.GIDLp = dokumentinfo.GIDLp;
                            bladapi = cdn_api.cdn_api.XLUruchomFormatkeWgGID(grupinfo);
                            if (bladapi != 0)
                            {
                                #region bladopis
                                string bladopis = "";
                                if (bladapi == 2)
                                    bladopis = "ustalono wybór z listy, a brak ustawienia trybu selekcji";
                                if (bladapi == 3)
                                    bladopis = "błąd otwarcia tabeli";
                                if (bladapi == 4)
                                    bladopis = "nie znaleziono rekordu o podanym GIDzie";
                                if (bladapi == 5)
                                    bladopis = "ustalono wybór elementu z listy a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 6)
                                    bladopis = "ustalono wybór grupy z listy, a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 7)
                                    bladopis = "ustalono podgląd elementów listy, a brak procedury listy dla takiego typu dokumentu";
                                if (bladapi == 8)
                                    bladopis = "nieobsługiwany GIDTyp";
                                #endregion
                                Loguj("Blad podonoszenia dokumentu XL. Blad XLUruchomFormatkeWgGID: " + bladapi.ToString() + " " + bladopis);
                            }
                        }
                        #endregion
                        //    ----------     ----------     ----------     ----------
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                res = false;
                Loguj("Blad: " + ex.Message);
                if (Xl != null)
                    if (Xl.IdSesji > 1)
                    {
                        Xl.Logout();
                        Loguj("Wylogowano xl");
                    }
            }
        }

        private void WykonajMetodeDlaWymiaru(int numer, int opisId, DataRow dr)
        {
            try
            {
                //DataRow drRejestr = polaczenie.PodajRejestr(((int)dr["dpk_dpId"]));
                string nazwaPolaId = string.Format("dpk_Wymiar{0}_ID", numer);
                string nazwaPolaNazwa = string.Format("Wymiar{0}_Nazwa", numer);
                string nazwaPolaKod = string.Format("dpk_Wymiar{0}_Kod", numer);
                string nazwaPolaWymagalny = string.Format("dpk_wymiar{0}_WartoscWymagana", numer);
                bool wymagana = dr.IsNull(nazwaPolaWymagalny) ? false : Convert.ToBoolean(dr[nazwaPolaWymagalny]);


                if ((dr.IsNull(nazwaPolaId)))
                    return;

                if (dr.IsNull(nazwaPolaKod))
                {
                    if (wymagana)
                        throw new Exception(string.Format("Wartość wymiaru nr: {0} nie może być pusta.", numer));
                    else
                        return;
                }

                XLDodajWymiarOpisuInfo_20163 dodajwymiaropisuinfo = new XLDodajWymiarOpisuInfo_20163();
                dodajwymiaropisuinfo.Wersja = 20163;
                dodajwymiaropisuinfo.Tryb = 0;//?
                //System.Windows.Forms.MessageBox.Show(drlk[nazwaPolaNazwa].ToString());
                int typ = SprawdzTypWymiaruAnalitycznego(dr[nazwaPolaNazwa].ToString());
                dodajwymiaropisuinfo.Typ = typ;// == 4 ? 0 : typ;
                DataTable dtRodzaj = null;
                string zapytanie = "";
                try
                {
                    if ((typ == 0) || (typ == 4))
                    {
                        dtRodzaj = polaczenie2.SprawdzRodzajWymiaru(dr[nazwaPolaNazwa].ToString(), ref zapytanie);
                        if ((dtRodzaj.Rows.Count == 0) || (dtRodzaj.Rows.Count > 1))
                            throw new Exception(string.Format("Nie znaleziono rodzaju wymiaru lub rodzaj wymiaru niejednoznaczny.{0}.", dtRodzaj.Rows.Count));
                    }
                }
                catch (Exception e1)
                {
                    throw new Exception(string.Format("Pierwsza część funkcji: {0}.", e1.Message));
                }
                bool isMember = false;
                try
                {
                    /*
                    System.Windows.Forms.MessageBox.Show(string.Format("Typ: {0}.[0][0]={1}.[0][1]={2}.Zapytanie={3}", typ, dtRodzaj.Rows[0][0].ToString(),
                        dtRodzaj.Rows[0][1].ToString(),zapytanie));
                    int w1 = Convert.ToInt32(dtRodzaj.Rows[0][0].ToString());
                    int w2 = Convert.ToInt32(dtRodzaj.Rows[0][1].ToString());
                     */


                    if (((typ == 0) || (typ == 4)) && (Convert.ToInt32(dtRodzaj.Rows[0][0].ToString()) == 2))
                    {

                        dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtRodzaj.Rows[0][1].ToString());
                        dodajwymiaropisuinfo.Element = dr[nazwaPolaKod].ToString();
                        isMember = true;
                    }
                }
                catch (Exception e1)
                {
                    throw new Exception(string.Format("Druga część funkcji: {0}.", e1.Message));
                }
                try
                {
                    if (!((typ == 0) || (typ == 4)) || (((typ == 0) || (typ == 4)) && (Convert.ToInt32(dtRodzaj.Rows[0][0].ToString()) == 1)))
                    {
                        DataTable dtlw = polaczenie2.SprawdzIdWymiaru(dr[nazwaPolaKod].ToString(), typ, dtRodzaj == null ? 0 : (int)dtRodzaj.Rows[0][1]);
                        if (dtlw.Rows.Count == 0)
                            throw new Exception("Nie znaleziono wartości wymiaru: " + dr[nazwaPolaKod].ToString() + " Typ: " + typ);
                        if (dtlw.Rows.Count > 1)
                            throw new Exception("Nazwa niejednoznacznie wskazuje na wymiar. Nazwa: " + dr[nazwaPolaKod].ToString());
                        dodajwymiaropisuinfo.Wymiar = Convert.ToInt32(dtlw.Rows[0][0].ToString());
                        isMember = true;
                    }
                }
                catch (Exception e1)
                {
                    throw new Exception(string.Format("Trzecia część funkcji: {0}.", e1.Message));
                }

                if (!isMember)
                    throw new Exception(string.Format("Niepoprawne warunki wejściowe dla wymiaru,typ={0},rodzaj={1}.", typ, (int)dtRodzaj.Rows[0][0]));




                int bladapi = 0;
                //dodajwymiaropisuinfo.Element = drlk["dpk_Wymiar1_Kod"].ToString();
                //Loguj("Typ: " + dodajwymiaropisuinfo.Typ + ", Wymiar: " + dodajwymiaropisuinfo.Wymiar);//
                bladapi = cdn_api.cdn_api.XLDodajWymiarOpisu(opisId, dodajwymiaropisuinfo);
                if (bladapi != 0) { Loguj("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); res = false; throw new Exception("Funkcja: XLDodajWymiarOpisu. Blad api: " + bladapi); }
            }
            catch (Exception exp)
            {
                throw new Exception(string.Format("Błąd dodawania opisu analitycznego. Wymiar: {0}.{1}.", numer, exp.Message));
            }
        }

        private void EdokKsieguj()
        {
            Loguj("EDOK Ksieguj");
            try
            {
                polaczenie.UpdateZaksiegujDokumentyPrzychodzace(DateTime.Now, Bll.operatorzy.Zalogowany.ID, 7, idtag);
                Loguj("Zmodyfikowano dokument na zaksiegowany");
                string opisZadrzenia = string.Format("Zaksięgowane przez: {0}.", Bll.operatorzy.Zalogowany.Imie + " " + Bll.operatorzy.Zalogowany.Nazwisko);
                Type typ = typeof(Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy);//System.Reflection.Assembly.GetExecutingAssembly().GetType("Ingenes.eDokument.Dokumenty.FormOtworzDokumentPrzychodzacy"); //
                System.Reflection.FieldInfo dynField = typ.GetField("ctrlDokument", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );
                if (dynField == null)
                    Loguj("Blad: Nie znaleziono pola: ctrlDokument");
                Ingenes.eDokument.RozpoznawanieDokumentow.CtrlDokument _ctrldok = (Ingenes.eDokument.RozpoznawanieDokumentow.CtrlDokument)dynField.GetValue(Parent);
                typ = typeof(Ingenes.eDokument.RozpoznawanieDokumentow.CtrlDokument);
                dynField = typ.GetField("meDecyzjaKomentarz", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (dynField == null)
                    Loguj("Blad: Nie znaleziono pola: meDecyzjaKomentarz");
                DevExpress.XtraEditors.MemoEdit _memokomentarz = (DevExpress.XtraEditors.MemoEdit)dynField.GetValue(_ctrldok);  
                Loguj("Pobrano komentarz");
                polaczenie.InsertZaksiegujDokumentyPrzychodzaceZdarzenia(idtag, (int)_TypZdarzenia.ZaksiegowanieDokumentu, Bll.operatorzy.Zalogowany.ID, DateTime.Now, opisZadrzenia, _memokomentarz.Text);
                Loguj("Dodano wpis zdarzenia ZaksiegowanieDokument");
                typ = typeof(Ingenes.eDokument.Dokumenty.FormDokumentPrzychodzacy);
                System.Reflection.FieldInfo dynFieldDokument = typ.GetField("_dokument", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (dynFieldDokument == null)
                    Loguj("Blad: Nie znaleziono pola: _dokument");
                DokumentPrzychodzacy _dp = (DokumentPrzychodzacy)dynFieldDokument.GetValue(Parent);
                dynField = typ.GetField("_rejestrPrzychodzacy", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (dynField == null)
                    Loguj("Blad: Nie znaleziono pola: _rejestrPrzychodzacy");
                RejestrPrzychodzacy _rp = (RejestrPrzychodzacy)dynField.GetValue(Parent);
                //dynField = typ.GetField("_ctrlListaDp", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                //Ingenes.eDokument.RozpoznawanieDokumentow.CtrlListaDokumentowPrzychodzacych _ctrlListaDp = (Ingenes.eDokument.RozpoznawanieDokumentow.CtrlListaDokumentowPrzychodzacych)dynField.GetValue(Parent);
                //Loguj("test-WczytajObiekt");

                System.Reflection.MethodInfo dynMethod = typ.GetMethod("WczytajObiektFull", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Instance);
                if (dynMethod == null)
                    Loguj("Blad: Nie znaleziono - WczytajObiektFull");
                try
                {
                    object[] temp = dynMethod.Invoke(Parent, new object[] { (object)_dp.ID, null }) as object[];
                    _dp = temp[0] as DokumentPrzychodzacy;
                    dynFieldDokument.SetValue(Parent, _dp);
                }
                catch (TargetInvocationException rwoex) 
                {
                    Loguj("Blad: WczytajObiekt: " + rwoex.InnerException.Message);
                }
                //((Ingenes.eDokument.Dokumenty.FormOtworzDokumentPrzychodzacy)Parent).WczytajObiekt(_dp, _rp, null);
                dynField = typ.GetField("_dokument", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                if (dynField == null)
                    Loguj("Błąd: Nie znaleziono pola: _dokument");
                //_dp = (DokumentPrzychodzacy)dynField.GetValue(Parent);
                System.Reflection.MethodInfo dynMethod2 = typ.GetMethod("OdswiezDokument", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

                

                ////Przywrócić odświeżanie //ZZXX  //!!!!!   //1.8.4
                try
                {
                    dynMethod2.Invoke(Parent,new object[] { Type.Missing, Type.Missing });
                }
                catch (TargetInvocationException rodex)
                {
                    Loguj("Blad: OdswiezDokument: " + rodex.InnerException.Message);
                }


                //((Ingenes.eDokument.Dokumenty.FormOtworzDokumentPrzychodzacy)Parent).OdswiezDokument(_dp);
            }
            catch (Exception exedks)
            {
                Loguj("Blad ksieowania: " + exedks.Message);
                res = false;
            }         
        }


        private void WycofajXL()
        {
            Loguj("Wycofywanie XL");
            try
            {
                cdn_api.XLOtwarcieNagInfo_20163 otwarcieinfo = new XLOtwarcieNagInfo_20163();
                otwarcieinfo.Wersja = 20163;
                otwarcieinfo.Tryb = 2;  // 1 - interakcyjny, 2 - wsadowy
                otwarcieinfo.GIDFirma = dokumentinfo.GIDFirma;
                otwarcieinfo.GIDTyp = dokumentinfo.GIDTyp;
                otwarcieinfo.GIDNumer = dokumentinfo.GIDNumer;
                otwarcieinfo.GIDLp = dokumentinfo.GIDLp;
                int bladapi = cdn_api.cdn_api.XLOtworzDokument(Xl.IdSesji, ref dok, otwarcieinfo);
                if (bladapi != 0)
                    throw new Exception("Funkcja XLOtworzDokument, blad api nr: " + bladapi);

                Xl.ZamknijDokument(dok, -1);    // -1 - skasowanie

                Loguj("Wycofano - Skasowano dokument.");

                Loguj("Usuwanie numeru dokumentu z bazy edok. (Id: " + idtag + ")");
                //polaczenie.UpdateXLNumer(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna,null, idtag);
                UpdejtujNumerDokumentu(KonfiguracjaRejestrIED.XLNumerDokumentu_kolumna, null);
            }
            catch (Exception exwxl)
            {
                Loguj("Blad wycofania xl: " + exwxl.Message);
            }
        }


        private void WylogujXL() 
        {
            //    ----------     ----------     ----------     ----------
            #region XlLogout
            try
            {
                if (Konfiguracja.WylogujXlApi)
                {
                    Xl.Logout();
                    Loguj(98);
                    Loguj("Wylogowano XL");
                    Loguj(100);
                }
            }
            catch { }
            #endregion
            //    ----------     ----------     ----------     ----------
        }


        private int SprawdzTypWymiaruAnalitycznego(string nazwawymiaru)
        {
            int res = 0;

            string[] TabNazwyStrukturaFirmy = "Centrum".Split(',');
            if (TabNazwyStrukturaFirmy.Contains(nazwawymiaru))
                res = 1;
            string[] TabNazwyLokalizacja = "Lokalizacja".Split(',');
            if (TabNazwyLokalizacja.Contains(nazwawymiaru))
                res = 2;
            string[] TabNazwyKontrahentDocelowy = "Kontrahent".Split(',');
            if (TabNazwyKontrahentDocelowy.Contains(nazwawymiaru))
                res = 3;
            if (nazwawymiaru.Contains("Kategoria."))
                res = 4;
            /*
            string[] TabNazwyKategoriaFinansowa = "Kategoria".Split(',');
            if (TabNazwyKategoriaFinansowa.Contains(nazwawymiaru))
                res = 4;
             */
            /*
            string[] TabNazwyKategoriaFinansowa = "Rodzaj kosztu".Split(',');
            if (TabNazwyKategoriaFinansowa.Contains(nazwawymiaru))
                res = 4;
             */
            string[] TabNazwyProjekt = "Projekt".Split(',');
            if (TabNazwyProjekt.Contains(nazwawymiaru))
                res = 5;

            return res;
        }


        private int DateToClariion(DateTime? data)
        {
            try
            {
                data = new DateTime(data.Value.Year, data.Value.Month, data.Value.Day);
                return Convert.ToInt32(System.Convert.ToDateTime(data).ToOADate()) + 36161;
            }
            catch (Exception exdac)
            {
                throw new Exception("Błąd podczas konwersji daty do clarion. " + exdac.Message);
            }
        }


        private void Loguj(string tekst)
        {
            tekstLog += tekst + Environment.NewLine;
            if (_mainForm != null)
            {
                (_mainForm.Controls["tbLog"] as TextBox).Text = tekstLog;
                _mainForm.Odswiez();
                _mainForm.Refresh();
            }
        }


        private void Loguj(int postep)
        {
            if (_mainForm != null)
            {
                (_mainForm.Controls["progressBar1"] as System.Windows.Forms.ProgressBar).Value = postep;
                _mainForm.Odswiez();
            }
        }


        private void ZapiszLog()
        {
            try
            {
                string logPath = Application.StartupPath + "\\PluginIED2XL_log.txt";
                string slog = "";
                if (File.Exists(logPath))
                    slog = File.ReadAllText(logPath);
                else
                    File.Create(logPath);
                slog += "\r\n\r\n[" + DateTime.Now.ToString() + "]\r\n";
                string nlog = slog + tekstLog;
                File.WriteAllText(logPath, nlog);
            }
            catch { }
        }


        //    ----------     ----------     ----------     ----------
        #region IEdokumentPlugin

        private string _guid = "F7CEFCA5-1D33-4F6A-9A0A-5416FAC10430";
        public string Guid { get { return _guid; } }
        private string _name = "IED2XL";
        public string Name { get { return _name; } }
        private string _description = "EDOKDPToXLFS";
        public string Description { get { return _description; } }
        private string _author = "INGENES";
        public string Author { get { return _author; } }
        private string _version = "1.2";
        public string Version { get { return _version; } }
        private string _typ = "DokumentPrzychodzacy->Ksiegowanie";
        public string Typ { get { return _typ; } }

        public bool BeforeExecute()
        {
            Parent.Invoke(new MethodInvoker(() =>
                {
                    try { Start(); }
                    catch { res = false; }
                }));
            return res;
        }
        public bool AfterExecute()
        {
            Parent.Invoke(new MethodInvoker(() =>
                {
                    Koniec();
                }));
            return res;
        }

        public eDokumentBLL Bll { set; get; }
        public object Tag { get; set; }
        public string KonfiguracjaGlobalna { set; get; }
        public string KonfiguracjaLokalna { set; get; }
        private string _Message = "";
        public string Message { get { return _Message; } }
        public Control Parent { set; get; }
        public IEdokumentPluginHost Host { get; set; }
        public void Initialize()
        {

        }
        public void Dispose()
        { }

        #endregion
        //    ----------     ----------     ----------     ----------


    }
}