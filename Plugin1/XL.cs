﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cdn_api;

namespace Plugin1
{
    class XL
    {
        private XLLoginInfo_20163 xlLoginInfo = new XLLoginInfo_20163();

        private int wersja = 20163;
        public int Wersja { get { return wersja; } }

        private int idSesji = 0;
        public int IdSesji { get { return idSesji; } }

        private int bladApi = 0;
        public int BladApi { get { return bladApi; } }

        public string OpisBlenduApi(int NumerFunkcji, int NumerBlendu) 
        {
            try
            {
                string res = "";
                if (NumerFunkcji == 0) 
                {
                    if (NumerBlendu == -8)
                        return "nie podano nazwy bazy";
                    if (NumerBlendu == -7)
                        return "baza niezarejestrowana w systemie";
                    if (NumerBlendu == -6)
                        return "nie podano hasła lub brak operatora";
                    if (NumerBlendu == -5)
                        return "nieprawidłowe hasło";
                    if (NumerBlendu == -4)
                        return "konto operatora zablokowane";
                    if (NumerBlendu == -3)
                        return "nie podano nazwy programu (pole ProgramID)";
                    if (NumerBlendu == -2)
                        return "błąd otwarcia pliku tekstowego, do którego mają być zapisywane komunikaty. Nie znaleziono ścieżki lub nazwa pliku jest nieprawidłowa.";
                    if (NumerBlendu == -1)
                        return "podano niepoprawną wersję API";
                    if (NumerBlendu == 0)
                        return "logowanie powiodło się";
                    if (NumerBlendu == 1)
                        return "inicjalizacja nie powiodła się";
                    if (NumerBlendu == 2)
                        return "występuje w przypadku, gdy istnieje już jedna instancja programu i nastąpi ponowne logowanie (z tego samego komputera i na tego samego operatora)";
                    if (NumerBlendu == 3)
                        return "występuje w przypadku, gdy istnieje już jedna instancja programu i nastąpi ponowne logowanie z innego komputera i na tego samego operatora, ale operator nie posiada prawa do wielokrotnego logowania";
                    if (NumerBlendu == 5)
                        return "występuje przy pracy terminalowej w przypadku, gdy operator nie ma prawa do wielokrotnego logowania i na pytanie czy usunąć istniejące sesje terminalowe wybrano odpowiedź ‘Nie’.";
                    if (NumerBlendu == 61)
                        return "błąd zakładania nowej sesji";
                }
                XLKomunikatInfo_20163 kominfo = new XLKomunikatInfo_20163();
                kominfo.Wersja = 20163;
                kominfo.Funkcja = NumerFunkcji;
                kominfo.Blad = NumerBlendu;
                kominfo.Tryb = 1;
                int blad = cdn_api.cdn_api.XLOpisBledu(kominfo);
                res = kominfo.OpisBledu;
                return res;
            }
            catch 
            {
                return "Blad xl api nr.: " + NumerBlendu.ToString();
            }
        }

        #region Logowanie

        public void Login(string programID, string baza, string opeIdent, string opeHaslo, string serwerKlucza,int TrybWsadowy)
        {
            try
            {
                xlLoginInfo.Wersja = 20163;
                xlLoginInfo.ProgramID = programID;

                xlLoginInfo.TrybWsadowy = TrybWsadowy;//1 - Wsadowy 0 - Okienka
                xlLoginInfo.Winieta = -1;// 0;
                xlLoginInfo.UtworzWlasnaSesje = 1;

                //xlLoginInfo.TrybWsadowy = 0;
                //xlLoginInfo.Winieta = -1;
                xlLoginInfo.UtworzWlasnaSesje = 0;

                //xlLoginInfo.PlikLog = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\INGENES\\HTL.ADC\\logxl.txt";

                xlLoginInfo.Baza = baza;
                xlLoginInfo.OpeIdent = opeIdent;
                xlLoginInfo.OpeHaslo = opeHaslo;
                xlLoginInfo.SerwerKlucza = serwerKlucza;

                int kodBledu = cdn_api.cdn_api.XLLogin(xlLoginInfo, ref this.idSesji);
                if (idSesji == 0)
                    Other.PokazKomunikat("Nie pobrano sesji.");
                if (kodBledu != 0)
                {
                    this.idSesji = 0;
                    //throw new XLApiException("XLApiSesja.Login", 0, kodBledu);
                    throw new Exception(OpisBlenduApi(0, kodBledu));
                }
                
            }
            catch (Exception e1)
            {
                throw new Exception("Wystąpił błąd podczas logowania do XL." + e1.Message);
            }
        }

        public void Logout()
        {
            if (idSesji == 0)
                return;
            int kodBledu = cdn_api.cdn_api.XLLogout(this.idSesji);

            if (kodBledu != 0)
                throw new Exception("Błąd podcas wylogowywania XL." + kodBledu);
            this.idSesji = 0;
        }

        #endregion

        #region Dokumenty Handlowe

        public int WystawDokument(XLDokumentNagInfo_20163 dokument)
        {
            try
            {
                int kodBledu;
                dokument.Wersja = 20163;
                int res = -1;
                kodBledu = cdn_api.cdn_api.XLNowyDokument(idSesji, ref res, dokument);
                if (kodBledu != 0) 
                    throw new Exception(OpisBlenduApi(1,kodBledu));
                return res;
            }
            catch (Exception e1)
            {
                throw new Exception("Wystąpił błąd podczas wystawiania dokumentu XL." + e1.Message);
            }
        }

        public int WystawDokument(int typ, string akronim)
        {
            XLDokumentNagInfo_20163 dokument = new XLDokumentNagInfo_20163();
            dokument.Typ = typ;
            dokument.Akronim = akronim;
            return WystawDokument(dokument);
        }

        public int WystawDokument(XLDokumentNagInfo_20163 dokument, ref int nrblendu)
        {
            int kodBledu;
            dokument.Wersja = 20163;
            int res = -1;
            kodBledu = cdn_api.cdn_api.XLNowyDokument(idSesji, ref res, dokument);
            nrblendu = kodBledu;
            if (kodBledu != 0) throw new Exception("Kod błędu xl: " + kodBledu.ToString() + " " + OpisBlenduApi(1, kodBledu));
            return res;
        }

        public void DodajVAT(int DokumentID, XLVatInfo_20163 vatinfo)
        {
            int kodBledu;
            vatinfo.Wersja = 20163;
            kodBledu = cdn_api.cdn_api.XLDodajVAT(DokumentID, vatinfo);
            if (kodBledu != 0) throw new Exception("Kod błędu xl: " + kodBledu.ToString() + " " + OpisBlenduApi(5, kodBledu));
        }

        public int OtworzDokument(XLOtwarcieNagInfo_20163 dokument)
        {
            try
            {
                int kodBledu;
                dokument.Wersja = 20163;
                int res = -1;
                kodBledu = cdn_api.cdn_api.XLOtworzDokument(idSesji, ref res, dokument);
                if (kodBledu != 0)
                    throw new Exception(OpisBlenduApi(37, kodBledu));
                return res;
            }
            catch (Exception e1)
            {
                throw new Exception("Wystąpił błąd podczas otwierania dokumentu XL." + e1.Message);
                return -1;
            }
        }

        public int OtworzDokument(int GIDNumer, int GIDTyp)
        {
            XLOtwarcieNagInfo_20163 dokument = new XLOtwarcieNagInfo_20163();
            dokument.GIDNumer = GIDNumer;
            dokument.GIDTyp = GIDTyp;
            return OtworzDokument(dokument);
        }

        public void ZamknijDokument(int idDok, XLZamkniecieDokumentuInfo_20163 zamInfo)
        {
            int kodBledu;
            zamInfo.Wersja = 20163;
            kodBledu = cdn_api.cdn_api.XLZamknijDokument(idDok, zamInfo);
            if (kodBledu != 0)
                throw new Exception(OpisBlenduApi(7, kodBledu));
        }

        public void ZamknijDokument(int idDok)
        {
            XLZamkniecieDokumentuInfo_20163 zamInfo = new XLZamkniecieDokumentuInfo_20163();
            zamInfo.Tryb = 0;//0 - zatwierdzony, 1 - do bufora, -1 - do skasowania
            zamInfo.GidNumer = 0;
            ZamknijDokument(idDok, zamInfo);
        }

        public void ZamknijDokument(int idDok, int Tryb)
        {
            XLZamkniecieDokumentuInfo_20163 zamInfo = new XLZamkniecieDokumentuInfo_20163();
            zamInfo.Tryb = Tryb;//0 - zatwierdzony, 1 - do bufora, -1 - do skasowania
            zamInfo.GidNumer = 0;
            ZamknijDokument(idDok, zamInfo);
        }

        public void DodajPozycje(int IdDokumentu, XLDokumentElemInfo_20163 element)
        {
            try 
            {
                int kodBledu;
                element.Wersja = 20163;
                kodBledu = cdn_api.cdn_api.XLDodajPozycje(IdDokumentu, element);
                if (kodBledu != 0) 
                {
                    throw new Exception(OpisBlenduApi(2, kodBledu));
                }
            }
            catch (Exception ex) 
            {
                throw new Exception("Wystąpił błąd podczas dodawania pozycji do dokumentu XL." + ex.Message);
            }
        }

        public void ModyfikujPozycje(int dokument, XLModyfikujElemInfo_20163 pozycja)
        {
            int kodBledu;
            pozycja.Wersja = 20163;
            kodBledu = cdn_api.cdn_api.XLModyfikujPozycje(dokument, pozycja);
            if (kodBledu != 0)
                throw new Exception(OpisBlenduApi(72, kodBledu));
        }

        public void ModyfikujPozycje(int dokument, int lp, string ilosc, string cena, string cenaP, string wartosc, string vat, string waluta)
        {
            XLModyfikujElemInfo_20163 pozycja = new XLModyfikujElemInfo_20163();
            pozycja.Ilosc = ilosc;
            pozycja.EleLp = lp;
            pozycja.Cena = cena;
            pozycja.CenaP = cenaP;
            pozycja.Wartosc = wartosc;
            pozycja.Vat = vat;
            pozycja.Waluta = waluta;
            ModyfikujPozycje(dokument, pozycja);
        }

        #endregion

        #region Nowe

        public void ZmienKontekstOperatora(int KontekstOperatoraId) 
        {
            cdn_api.XLZmianaKontekstuOperatora_20163 zmkontekstu = new XLZmianaKontekstuOperatora_20163();
            zmkontekstu.Wersja = wersja;
            zmkontekstu.Id = KontekstOperatoraId;
            ZmienKontekstOperatora(zmkontekstu);

        }

        public void ZmienKontekstOperatora(cdn_api.XLZmianaKontekstuOperatora_20163 zmkontekstu) 
        {
            bladApi = cdn_api.cdn_api.XLZmianaKontekstuOperatora(zmkontekstu);
        }

        #endregion


    }
}
